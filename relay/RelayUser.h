//
// Created by bso on 16.03.18.
//

#ifndef UNITY_RELAYUSER_H
#define UNITY_RELAYUSER_H

#include <peer/UserContext.h>

namespace relay {

using namespace core;

enum RelayClientStates {
	StateWaitConnect = 0,
	StateWork,
	StateFinished
};

class RelayUser: public UserContext {
public:
	explicit RelayUser(const upack::UserInfo &fwd);

	~RelayUser();

	void onConnected(const std::shared_ptr<ISocketChannel> &socket_channel);

	void run();

	std::shared_ptr<RelayUser> shared_relay_user();

	void onMessage(const std::string &data, size_t size, uint16_t pid, uint16_t type) final;

	std::shared_ptr<ISocketChannel> m_socket_channel;

private:
	template <typename T>
	void handlePacket(const std::string &data);

	boost::asio::deadline_timer m_timer;

	uint16_t m_state = StateWaitConnect;
};

}

#endif //UNITY_RELAYUSER_H
