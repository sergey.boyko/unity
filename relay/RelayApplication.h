//
// Created by bso on 11.03.18.
//

#ifndef UNITY_RELAYAPPLICATION_H
#define UNITY_RELAYAPPLICATION_H

#include <IApplication.h>
#include <PeerConnection.h>
#include <ManyFacedListener.h>
#include <ManyFacedCaller.h>
#include <upack.pb.h>

namespace relay {

using namespace core;

class RelayUser;

class RelayApplication: public IApplication {
public:
	RelayApplication();

	void run() final;

	void Restart() final;

	void Stop() final;

	static RelayApplication *GetInstance() {
		return Instance;
	}

	template <typename T>
	void handlePacket(const std::string &data);

	void SendMessageToMainServer(uint16_t pid,
								 const google::protobuf::Message &message);

	void OnWaitClientConnectionDeadline(const std::shared_ptr<RelayUser> &user);

	/**
	* Other relays
	* first - socket_channel_id
	* second - UserContext
	*/
	std::unordered_map<uint32_t, std::shared_ptr<ISocketChannel>> m_other_relays;

	/**
	* first - cookie
	* second - UserContext
	*/
	std::unordered_map<std::string, std::shared_ptr<RelayUser>> m_user_peers;

private:
	void processPacket(const upack::RelayConnectResponse &fwd);

	void processPacket(const upack::NotificationToRelayAboutNewUser &fwd);

	struct MainServerListener: public PeerConnection {
		explicit MainServerListener(boost::asio::io_service &io);

		void onConnect(const std::shared_ptr<ISocketChannel> &socket_channel) final;

		void onMessage(const std::shared_ptr<ISocketChannel> &socket_channel,
					   const std::string &data,
					   size_t size,
					   uint16_t pid,
					   uint16_t type) final;

		void onLostConnection(const std::shared_ptr<ISocketChannel> &socket_channel) final;

		void onFinished(const std::shared_ptr<ISocketChannel> &socket_channel) final;
	};

	struct OtherRelayServerCaller: public ManyFacedCaller {
		explicit OtherRelayServerCaller(boost::asio::io_service &io,
										const std::vector<tcp::endpoint> &addresses,
										bool need_ping);

		void onConnect(const std::shared_ptr<ISocketChannel> &socket_channel) final;

		void onMessage(const std::shared_ptr<ISocketChannel> &socket_channel,
					   const std::string &data,
					   size_t size,
					   uint16_t pid,
					   uint16_t type) final;

		void onLostConnection(const std::shared_ptr<ISocketChannel> &socket_channel) final;

		void onFinished(const std::shared_ptr<ISocketChannel> &socket_channel) final;
	};

	struct OtherRelayServerListener: public ManyFacedListener {
		explicit OtherRelayServerListener(boost::asio::io_service &io,
										  uint32_t listen_port);

		void onConnect(const std::shared_ptr<ISocketChannel> &socket_channel) final;

		void onMessage(const std::shared_ptr<ISocketChannel> &socket_channel,
					   const std::string &data,
					   size_t size,
					   uint16_t pid,
					   uint16_t type) final;

		void onLostConnection(const std::shared_ptr<ISocketChannel> &socket_channel) final;

		void onFinished(const std::shared_ptr<ISocketChannel> &socket_channel) final;
	};

	struct ClientPeersListener: public ManyFacedListener {
	public:
		explicit ClientPeersListener(boost::asio::io_service &io, const tcp::endpoint &listen_ep);

		void onConnect(const std::shared_ptr<ISocketChannel> &socket_channel) final;

		void onMessage(const std::shared_ptr<ISocketChannel> &socket_channel,
					   const std::string &data,
					   size_t size,
					   uint16_t pid,
					   uint16_t type) final;

		void onLostConnection(const std::shared_ptr<ISocketChannel> &socket_channel) final;

		void onFinished(const std::shared_ptr<ISocketChannel> &socket_channel) final;
	};

	std::shared_ptr<MainServerListener> m_main_server;
	std::shared_ptr<OtherRelayServerCaller> m_other_relay_caller;
	std::shared_ptr<OtherRelayServerListener> m_other_relay_listener;
	std::shared_ptr<ClientPeersListener> m_clients_listener;

	static RelayApplication *Instance;

	bool m_is_begins = true;
};

#define App() RelayApplication::GetInstance()

}

#endif //UNITY_RELAYAPPLICATION_H
