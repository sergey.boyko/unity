//
// Created by bso on 16.03.18.
//

#include <ISocketChannel.h>

#include "RelayUser.h"
#include "RelayApplication.h"
#include "RelayEnum.h"

namespace relay {

RelayUser::RelayUser(const upack::UserInfo &fwd):
		UserContext(fwd),
		m_timer(App()->GetIO()) {
	LOG_D() << "RelayUser() m_uid = " << m_uid;
}


void RelayUser::onConnected(const std::shared_ptr<ISocketChannel> &socket_channel) {
	LOG_D() << "onConnected() m_uid = " << m_uid;
	m_socket_channel = socket_channel;
	m_state = StateWork;
	m_timer.cancel();

	upack::ClientConnected fwd1;
	FillMessage(fwd1.mutable_m_user());
	App()->SendMessageToMainServer(upack::pClientConnected, fwd1);

	upack::UserConnectResponse fwd2;
	fwd2.set_m_success(true);
	socket_channel->SendPacket(upack::pUserConnectResponse, fwd2);
}


RelayUser::~RelayUser() {
	LOG_D() << "~RelayUser() m_uid = " << m_uid;
	m_timer.cancel();
}

void RelayUser::run() {
	m_timer.expires_from_now(boost::posix_time::seconds(WaitClientConnectionTimeout));
	m_timer.async_wait([=](const boost::system::error_code &ec) {
		if(ec) {
			return;
		}
		LOG_I() << "On wait connection deadline m_uid = " << m_uid;
		m_state = StateFinished;
		App()->OnWaitClientConnectionDeadline(shared_relay_user());
	});
}

std::shared_ptr<RelayUser> RelayUser::shared_relay_user() {
	return std::dynamic_pointer_cast<RelayUser>(shared_from_this());
}

void RelayUser::onMessage(const std::string &data, size_t size, uint16_t pid, uint16_t type) {
	LOG_D() << "AuthUser::AuthUser::onMessage() {" << m_uid << "} pid = "
			<< pid << " type = " << type;

	if(type != Protobuf) {
		//TODO
		return;
	}

	if(m_state == StateFinished) {
		return;
	}

	switch(pid) {

		default: {
			LOG_I() << "Received unknown packet";
			return;
		};
	}
}

}