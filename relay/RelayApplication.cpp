//
// Created by bso on 11.03.18.
//

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <CallerSocketChannel.h>
#include <PeerConnection.h>
#include <google/protobuf/util/json_util.h>

#include "Config.h"
#include "RelayApplication.h"
#include "RelayEnum.h"
#include "RelayUser.h"

namespace relay {

using namespace boost::asio::ip;

RelayApplication *RelayApplication::Instance = nullptr;

RelayApplication::RelayApplication():
		m_main_server(new MainServerListener(m_io)) {
	Instance = this;
}


void RelayApplication::run() {
	m_main_server->run();
	m_io.run();
}


void RelayApplication::Restart() {
	//TODO
}


void RelayApplication::Stop() {
	//TODO
}


template <typename T>
void RelayApplication::handlePacket(const std::string &data) {
	T fwd;
	if(fwd.ParseFromString(data)) {

		std::string json_output;
		google::protobuf::util::MessageToJsonString(fwd, &json_output);
		LOG_D() << "Received message: " << fwd.GetDescriptor()->full_name() << json_output;

		processPacket(fwd);
		return;
	}

	LOG_E() << "Couldn't parse packet";
}


void RelayApplication::processPacket(const upack::RelayConnectResponse &fwd) {
	LOG_D() << "ProcessPacket() RelayConnectResponse";

	if(m_is_begins) {
		m_other_relay_listener.reset(
				new OtherRelayServerListener(App()->GetIO(),
											 fwd.m_listen_other_relays_port()));
		m_other_relay_listener->run();

		std::vector<tcp::endpoint> relays;
		for(auto i = 0; i < fwd.m_relay_list().size(); ++i) {
			relays.emplace_back(ip::address::from_string(fwd.m_relay_list(i).m_ip()),
								fwd.m_relay_list(i).m_port());
		}

		m_other_relay_caller.reset(new OtherRelayServerCaller(App()->GetIO(),
															  relays,
															  false));
		m_other_relay_caller->run();

		m_clients_listener.reset(
				new ClientPeersListener(m_io, tcp::endpoint(tcp::v4(),
															fwd.m_listen_clients_port())));
		m_clients_listener->run();

		m_is_begins = false;
	} else {
		m_other_relay_listener->SetListenEndpoint(
				tcp::endpoint(tcp::v4(), fwd.m_listen_other_relays_port()));

		//It is assumed, that m_other_relay_caller already exists
		for(auto i = 0; i < fwd.m_relay_list().size(); ++i) {
			if(fwd.m_relay_list(i).m_is_new()) {
				auto address = tcp::endpoint(
						ip::address::from_string(fwd.m_relay_list(i).m_ip()),
						fwd.m_relay_list(i).m_port());
				m_other_relay_caller->AddPeer(address);
			}
		}

		m_clients_listener->SetListenEndpoint(tcp::endpoint(tcp::v4(),
															fwd.m_listen_clients_port()));
	}
}


void RelayApplication::processPacket(const upack::NotificationToRelayAboutNewUser &fwd) {
	LOG_D() << "ProcessPacket() NotificationToRelayAboutNewUser";
	m_user_peers[fwd.m_user().m_cookie()] = std::make_shared<RelayUser>(fwd.m_user());
	m_user_peers[fwd.m_user().m_cookie()]->run();

	upack::RelayFeedbackOnNewUser pack;
	pack.set_m_success(true);
	pack.set_m_uid(fwd.m_user().m_uid());
	SendMessageToMainServer(upack::pRelayFeedbackOnNewUser, pack);
}


void RelayApplication::SendMessageToMainServer(uint16_t pid,
											   const google::protobuf::Message &message) {
	m_main_server->SendPacket(pid, message);
}


void RelayApplication::OnWaitClientConnectionDeadline(const std::shared_ptr<RelayUser> &user) {
	m_user_peers.erase(user->m_cookie);
}


RelayApplication::MainServerListener::MainServerListener(boost::asio::io_service &io):
		PeerConnection(io,
					   tcp::endpoint(address::from_string(
							   Config["callers"]["main"]["ip"].asString()),
									 Config["callers"]["main"]["port"].asUInt()),
					   false) {
	LOG_D() << "MainServerListener()";
}


void RelayApplication::MainServerListener::onConnect(const std::shared_ptr<ISocketChannel> &socket_channel) {
	LOG_I() << "MainServerListener::onConnect() ";
	upack::RelayConnectRequest fwd;
	fwd.set_m_is_begins(App()->m_is_begins);
	//Send to Main server
	m_peer->SendPacket(upack::pRelayConnectRequest, fwd);
}


void RelayApplication::MainServerListener::onLostConnection(const std::shared_ptr<ISocketChannel> &socket_channel) {
	LOG_I() << "MainServerListener::onLostConnection()";
	m_reconnect_timer.expires_from_now(boost::posix_time::seconds(BAD_ACCEPT_TIMEOUT));
	m_reconnect_timer.async_wait([=](const boost::system::error_code &ec) {
		if(ec) {
			return;
		}
		m_peer->run();
	});
}


void RelayApplication::MainServerListener::onFinished(const std::shared_ptr<ISocketChannel> &socket_channel) {
	PeerConnection::onFinished(socket_channel);
}


void RelayApplication::MainServerListener::onMessage(
		const std::shared_ptr<ISocketChannel> &socket_channel,
		const std::string &data,
		size_t size,
		uint16_t pid,
		uint16_t type) {
	LOG_D() << "Received message from MainServer pid = " << pid;
	if(type == Protobuf) {
		switch(pid) {
			case upack::pRelayConnectResponse: {
				App()->handlePacket<upack::RelayConnectResponse>(data);
				return;
			}

			case upack::pNotificationToRelayAboutNewUser: {
				App()->handlePacket<upack::NotificationToRelayAboutNewUser>(data);
				return;
			}

			default: {
				LOG_D() << "Received unknown packet";
				return;
			};
		}
	}
}


RelayApplication::OtherRelayServerCaller::OtherRelayServerCaller(
		boost::asio::io_service &io,
		const std::vector<tcp::endpoint> &addresses,
		bool need_ping): ManyFacedCaller(io, addresses) {
	LOG_D() << "OtherRelayServerCaller()";
}


void RelayApplication::OtherRelayServerCaller::onConnect(const std::shared_ptr<ISocketChannel> &socket_channel) {
	App()->m_other_relays[socket_channel->m_id] = socket_channel;
	ManyFacedCaller::onConnect(socket_channel);
}


void RelayApplication::OtherRelayServerCaller::onMessage(
		const std::shared_ptr<ISocketChannel> &socket_channel,
		const std::string &data,
		size_t size,
		uint16_t pid,
		uint16_t type) {
	if(type == Protobuf) {
		switch(pid) {
			default: {
				LOG_E() << "Received unknown packet";
				return;
			}
		}
	}
}


void RelayApplication::OtherRelayServerCaller::onLostConnection(const std::shared_ptr<ISocketChannel> &socket_channel) {
	//To do nothing, because ManyFacedCaller will run this socket
	//ManyFacedCaller::onLostConnection(socket_channel);
}


void RelayApplication::OtherRelayServerCaller::onFinished(const std::shared_ptr<ISocketChannel> &socket_channel) {
	ManyFacedCaller::onFinished(socket_channel);
}


RelayApplication::OtherRelayServerListener::OtherRelayServerListener(
		boost::asio::io_service &io,
		uint32_t listen_port):
		ManyFacedListener(io,
						  tcp::endpoint(ip::tcp::v4(),
										listen_port),
						  RelayNeedPing) {
	LOG_D() << "OtherRelayServerListener()";
}


void RelayApplication::OtherRelayServerListener::onConnect(const std::shared_ptr<ISocketChannel> &socket_channel) {
	//TODO
	ManyFacedListener::onConnect(socket_channel);
}


void RelayApplication::OtherRelayServerListener::onMessage(
		const std::shared_ptr<ISocketChannel> &socket_channel,
		const std::string &data,
		size_t size,
		uint16_t pid,
		uint16_t type) {
	//TODO
	ManyFacedListener::onMessage(socket_channel, data, size, pid, type);
}


void RelayApplication::OtherRelayServerListener::onLostConnection(
		const std::shared_ptr<ISocketChannel> &socket_channel) {
	//TODO
	ManyFacedListener::onLostConnection(socket_channel);
}


void RelayApplication::OtherRelayServerListener::onFinished(const std::shared_ptr<ISocketChannel> &socket_channel) {
	//TODO
	ManyFacedListener::onFinished(socket_channel);
}


RelayApplication::ClientPeersListener::ClientPeersListener(boost::asio::io_service &io,
														   const tcp::endpoint &listen_ep):
		ManyFacedListener(io, listen_ep, ClientNeedPing) {
	LOG_D() << "ClientPeersListener()";
}


void RelayApplication::ClientPeersListener::onConnect(
		const std::shared_ptr<ISocketChannel> &socket_channel) {
	//FIXME to do nothing, because we wait message with cookie from user
	ManyFacedListener::onConnect(socket_channel);
}


void RelayApplication::ClientPeersListener::onLostConnection(
		const std::shared_ptr<ISocketChannel> &socket_channel) {
	ManyFacedListener::onLostConnection(socket_channel);
}


void RelayApplication::ClientPeersListener::onFinished(const std::shared_ptr<ISocketChannel> &socket_channel) {
	LOG_I() << "ClientPeersListener::onFinished() socket_channel_id = "
			<< socket_channel->m_id;
	for(auto &peer : App()->m_user_peers) {
		if(peer.second->m_socket_channel == socket_channel) {
			upack::ClientDisconnected fwd;
			fwd.set_m_uid(peer.second->m_uid);
			App()->SendMessageToMainServer(upack::pClientDisconnected, fwd);

			App()->m_user_peers.erase(peer.first);
			break;
		}
	}

	ManyFacedListener::onFinished(socket_channel);
}


void RelayApplication::ClientPeersListener::onMessage(
		const std::shared_ptr<ISocketChannel> &socket_channel,
		const std::string &data,
		size_t size,
		uint16_t pid,
		uint16_t type) {
	LOG_D() << "ClientPeersListener::onMessage() pid = " << pid;

	if(type != Protobuf) {
		//TODO
		return;
	}

	if(pid == upack::pUserConnectRequest) {
		upack::UserConnectRequest fwd;
		if(fwd.ParseFromString(data)) {
			for(auto &peer : App()->m_user_peers) {
				if(peer.first == fwd.m_cookie()) {
					peer.second->onConnected(socket_channel);
				}
			}
		}
	} else {
		for(auto &peer : App()->m_user_peers) {
			if(peer.second->m_socket_channel == socket_channel) {
				peer.second->onMessage(data, size, pid, type);
			}
		}
	}
}

}