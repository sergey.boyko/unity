//
// Created by bso on 10.12.17.
//

#include <fstream>
#include "Config.h"

namespace relay {

Json::Value Config;

void ConfigLoad(const std::string &config){
	Json::Reader reader;
	std::ifstream in(config);

	if(!reader.parse(in, Config)) {
		throw std::runtime_error("Could't parse config: " + config);
	}
}

}