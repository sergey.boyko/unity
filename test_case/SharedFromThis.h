//
// Created by bso on 09.03.18.
//

#ifndef UNITY_SHAREDFROMTHIS_H
#define UNITY_SHAREDFROMTHIS_H

#include <memory>

struct virtual_enable_shared_from_this_base:
		std::enable_shared_from_this<virtual_enable_shared_from_this_base> {
	virtual ~virtual_enable_shared_from_this_base() {}
};
template<typename T>
struct virtual_enable_shared_from_this:
		virtual virtual_enable_shared_from_this_base {
	std::shared_ptr<T> shared_from_this() {
		return std::dynamic_pointer_cast<T>(
				virtual_enable_shared_from_this_base::shared_from_this());
	}
};

struct Base1: virtual_enable_shared_from_this<Base1> {};
struct Base2: virtual_enable_shared_from_this<Base2> {};
struct SharedFromThis: Base1, Base2 { };


#endif //UNITY_SHAREDFROMTHIS_H
