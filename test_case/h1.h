//
// Created by bso on 17.10.17.
//

#ifndef UNITY_H1_H
#define UNITY_H1_H

#include <memory>

struct h2;

struct h1{
	int x=111;
	std::shared_ptr<h2> ptr2;
};

#endif //UNITY_H1_H
