//
// Created by bso on 17.10.17.
//

#ifndef UNITY_H2_H
#define UNITY_H2_H

#include <memory>

struct h1;

struct h2{
	int x=222;
	std::shared_ptr<h1> ptr1;
};

#endif //UNITY_H2_H
