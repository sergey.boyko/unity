//
// Created by bso on 14.10.17.
//

#include <ProgramLog.h>
#include <ProtoParser.h>
#include <google/protobuf/message.h>
#include <unistd.h>
#include <upack.pb.h>
#include <google/protobuf/util/json_util.h>
#include <ReceiverSocketChannel.h>
#include <CallerSocketChannel.h>
#include <ISocketListener.h>
#include <ManyFacedListener.h>
#include "h1.h"
#include "h2.h"
#include <ManyFacedListener.h>
#include <PeerConnection.h>

#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include <GlobalCommon.h>
#include "TestEqualityPtr.h"
#include "ThreadClass.h"
#include "SharedFromThis.h"

using namespace core;
using namespace upack;

class Connector: public ISocketListener {
public:
	explicit Connector() {
		LOG_D() << "Connector()";
	}

	~Connector() {
		LOG_D() << "~Connector()";
	}

	void onMessage(const std::shared_ptr<ISocketChannel> &socket_channel,
				   const std::string &data,
				   size_t size,
				   uint16_t pid,
				   uint16_t type) final {
		LOG_D() << "onMessage";
	}

	void onLostConnection(const std::shared_ptr<ISocketChannel> &) final {
		LOG_D() << "onLostConneciton";
	}

	void onFinished(const std::shared_ptr<ISocketChannel> &) final {
		LOG_D() << "onFinished";
	}

	void onConnect(const std::shared_ptr<ISocketChannel> &) final {
		LOG_D() << "onConnect";
	}
};

void MessageHeaderTest() {
	LOG_D() << "***MessageHeader***";
	LOG_D() << "Size of MessageHeader:" << sizeof(MessageHeader);

	ProtoParser p;
	std::string text_message = "hello, world";
	LOG_D() << "Size of string: \"" << text_message << "\" :" << text_message.size();

	auto packet = p.InsertHeader(text_message, 3);

	LOG_D() << "String after InsertHeader with pid 3: \"" << packet << "\"";
	LOG_D() << "Size of this string: " << packet.size();

	LOG_D() << "The size of Protobuf packet is 4? - " << p.CheckReceivedPacket(packet.data(), 4);
	LOG_D() << "The size of Protobuf packet is 20? - " << p.CheckReceivedPacket(packet.data(), 20);
	uint16_t pid, type;
	size_t size_output;
	auto new_data = p.EraseHeader(packet, size_output, pid, type);
	LOG_D() << "The packet id of message is " << pid;
	LOG_D() << "The packet type of message is " << type;
	LOG_D() << ". Packet after erase header: \"" << new_data << "\"";
}

void ProtoTest() {
	LOG_D() << "***Proto***";

	upack::Test message;
	message.set_m_text("It's proto packet");
	message.set_m_num1(3);
	message.set_m_num2(1231241);

	std::string proto_data = message.SerializeAsString();
	LOG_D() << "Message after serialize: \"" << proto_data << "\"";
	upack::Test message1;
	message1.ParseFromString(proto_data);
	LOG_D() << "Message.m_text after Parse to Proto: \"" << message1.m_text() << "\"";
	LOG_D() << "Message.m_num1 after Parse to Proto: " << message1.m_num1();
	LOG_D() << "Message.m_num2 after Parse to Proto: " << message1.m_num2();

	std::string json_output;
	google::protobuf::util::MessageToJsonString(message1, &json_output);
	LOG_D() << "After convert protobuf message to Json: " << message1.GetDescriptor()->full_name() << json_output;

	LOG_D() << "";
}

void ISocketChannelTest() {
	LOG_D() << "***ISocketChannel***";

	boost::asio::io_service io;
	tcp::endpoint ep;
	Connector *connector = new Connector();
	std::shared_ptr<CallerSocketChannel> chanel;
	chanel.reset(new CallerSocketChannel(io, connector, ep, 0, false));
	chanel->run();
	io.run();
}

void CrossReferencesTest() {
	LOG_D() << "***cross-references***";
	std::shared_ptr<h1> a1(new h1());
	std::shared_ptr<h2> a2(new h2());
	a1->ptr2 = a2;
	a2->ptr1 = a1;

	LOG_D() << "h1.ptr2->x = :" << a1->ptr2->x;
	LOG_D() << "h2.ptr2->x = :" << a2->ptr1->x;
}

void ManyFacedPeerTest() {
	LOG_D() << "***ManyFacedListener***";
	boost::asio::io_service io;
	boost::asio::ip::tcp::endpoint ep(boost::asio::ip::address::from_string("127.0.0.1"),
									  3000);
	ManyFacedListener x(io, ep, false);
	x.run();
	io.run();
}

void PeerConnectionTest() {
	LOG_D() << "***PeerConnectionTest***";
	boost::asio::io_service io;
	boost::asio::ip::tcp::endpoint ep(boost::asio::ip::address::from_string("127.0.0.1"),
									  3000);
	PeerConnection x(io, ep, false);
	x.run();
	io.run();
}

void MysqlConnectorTest() {
	LOG_D() << "***MysqlConnector***";
	sql::Driver *driver;
	sql::Connection *con;
	sql::Statement *stmt;
	sql::PreparedStatement *pstmt;
	sql::ResultSet *res;

	try {
		driver = get_driver_instance();
		con = driver->connect("tcp://127.0.0.1:3020", "root", "cthsq,jqrj1");
		con->setSchema("unity");

		//pstmt = con->prepareStatement("insert into auth (username,password,reg_date) values(?,?,now())");
		//pstmt->setString(1, "testuser");
		//pstmt->setString(1, "testpass");
		//if(pstmt->executeUpdate()) {
		//	LOG_D() << "Success insert";
		//} else {
		//	LOG_D() << "Insert failed";
		//}
		stmt = con->createStatement();
		//stmt->execute("INSERT INTO auth(username,password) VALUES (\"testuser\",\"testpas\")");
		pstmt = con->prepareStatement("SELECT UNIX_TIMESTAMP(reg_date) FROM auth WHERE uid = 1");
		res = pstmt->executeQuery();
		if(!res) {
			LOG_D() << "Invalid request";
			return;
		}
		//res = stmt->executeQuery("SELECT UNIX_TIMESTAMP(reg_date) FROM auth WHERE uid = 1");
		res->first();
		LOG_D() << "UNIX_TIMESTAMP = " << res->getInt64(1);

		std::time_t time = res->getInt64(1);
		struct tm *str = localtime(&time);
		LOG_D() << "Year = " << str->tm_year + 1900;
		LOG_D() << "Month = " << str->tm_mon + 1;
		LOG_D() << "Day = " << str->tm_mday;
	} catch(const sql::SQLException &er) {
		LOG_E() << er.what();
	}
}

void MysqlRegistrationTest() {
	LOG_D() << "***MysqlConnector***";
	sql::Driver *driver;
	sql::Connection *con;
	sql::Statement *stmt;
	sql::PreparedStatement *pstmt;
	sql::ResultSet *res;

	try {
		driver = get_driver_instance();
		con = driver->connect("tcp://127.0.0.1:3020", "root", "cthsq,jqrj1");
		con->setSchema("unity");

		//pstmt = con->prepareStatement("insert into auth (username,password,reg_date) values(?,?,now())");
		//pstmt->setString(1, "testuser");
		//pstmt->setString(1, "testpass");
		//if(pstmt->executeUpdate()) {
		//	LOG_D() << "Success insert";
		//} else {
		//	LOG_D() << "Insert failed";
		//}
		stmt = con->createStatement();
		stmt->execute("INSERT INTO auth(username,password) VALUES (\"testuser1\",\"testpas\")");
		res = stmt->getResultSet();
		if(!res) {
			LOG_D() << "Invalid request";
			return;
		}

	} catch(const sql::SQLException &er) {
		LOG_E() << er.what();
	}
}

void EqualitySharedPtr() {
	std::shared_ptr<Base> a;
	std::shared_ptr<Derived> b;
	std::shared_ptr<Base> c = b;

	if(b == c) {
		LOG_D() << "c = b";
		return;
	}

	LOG_D() << "c != b";
}

void ThreadClassTest() {
	boost::asio::io_service io;
	ThreadClass tc(io);
	tc.run();
	io.run();
}

int main(int argc, char *argv[]) {
	//ProgramLog::LogInit("test_case", "/home/bso/tmp/", true, getpid(), true);

	//LOG_D() << "Start test";

	////PeerConnectionTest();
	////MysqlRegistrationTest();
	//EqualitySharedPtr();

	auto ss = GetCookie(10);

	ThreadClassTest();
	return 0;
}