//
// Created by bso on 02.01.18.
//

#ifndef UNITY_THREADCLASS_H
#define UNITY_THREADCLASS_H

#include <mutex>
#include <boost/asio/deadline_timer.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <thread>

class ThreadClass {
public:
	ThreadClass(boost::asio::io_service &io):
			m_timer(io),
			m_thread(&ThreadClass::onStartThread, this) {
	}

	void run() {
		m_thread.detach();
		onTimer();
	}

	void onTimer() {
		m_timer.expires_from_now(boost::posix_time::millisec(300));
		m_timer.async_wait([=](const boost::system::error_code &er) {
			if(er) {
				return;
			}
			m_buf.push_back(std::to_string(m_buf.size()));
			onTimer();
		});
	}

	void onStartThread() {
		m_mutex.lock();
		std::string line;
		std::getline(std::cin, line);
		m_timer.cancel();

		for(auto it:m_buf) {
			std::cout << it << std::endl;
		}

		m_mutex.unlock();
	}

private:
	std::mutex m_mutex;
	std::thread m_thread;

	std::vector<std::string> m_buf;

	boost::asio::deadline_timer m_timer;
};


#endif //UNITY_THREADCLASS_H
