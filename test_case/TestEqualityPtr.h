//
// Created by bso on 10.12.17.
//

#ifndef UNITY_TESTEQUALITYPTR_H
#define UNITY_TESTEQUALITYPTR_H

#include <string>

class Base {
public:
	Base(): m_text1("text1") {
	}

	std::string m_text1;
};

class Derived: public Base {
public:
	Derived(): m_text2("text2") {
	}

	std::string m_text2;
};

#endif //UNITY_TESTEQUALITYPTR_H
