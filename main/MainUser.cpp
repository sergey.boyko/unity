//
// Created by bso on 13.03.18.
//

#include "MainUser.h"
#include "MainApplication.h"
#include "MainEnums.h"

namespace mainapp {

MainUser::MainUser(const upack::UserInfo &info):
		UserContext(info) {
}


std::shared_ptr<MainUser> MainUser::shared_main_user() {
	return std::dynamic_pointer_cast<MainUser>(shared_from_this());
}


void MainUser::onMessage(const std::string &data, size_t size, uint16_t pid, uint16_t type) {
	//TODO change info...
}

}