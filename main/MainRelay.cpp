//
// Created by bso on 13.03.18.
//

#include <ProgramLog.h>
#include "MainRelay.h"

namespace mainapp {

using namespace core;

MainRelay::MainRelay(const std::shared_ptr<ISocketChannel> &socket_channel,
					 uint16_t listen_other_relays_port,
					 uint16_t listen_clients_port,
					 bool is_new):
		m_socket_channel(socket_channel),
		m_listen_other_relays_port(listen_other_relays_port),
		m_listen_clients_port(listen_clients_port),
		m_is_new(is_new) {
	LOG_D() << "MainRelay() id = "
			<< m_socket_channel->m_id
			<< " listen_port = "
			<< listen_other_relays_port;
}


MainRelay::~MainRelay() {
	LOG_D() << "~MainRelay() id = " << m_socket_channel->m_id;
}

}