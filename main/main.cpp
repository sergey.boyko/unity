//
// Created by bso on 22.09.17.
//

#include <string>
#include <boost/program_options.hpp>
#include <boost/asio.hpp>
#include <iostream>
#include <ProgramLog.h>

#include "Config.h"
#include "MainApplication.h"

namespace po = boost::program_options;
using namespace core;
using namespace mainapp;

int main(int argc, char *argv[]) {
	std::string config;
	bool console_allowed = false;

	po::options_description general_options("General options");
	general_options.add_options()
			("help,h", "Show help")
			("daemon,d", "Make a daemon")
			("config,c", po::value<std::string>(&config)->required(),
			 "Enter path to the configuration file with the full file name")
			("console-allowed,a", "Make application is console allowed");

	po::variables_map vm;

	//try {
	po::store(po::parse_command_line(argc, argv, general_options), vm);

	if(vm.count("help")) {
		std::cout << general_options << std::endl;
		return 0;
	}

	po::notify(vm);

	if(vm.count("console-allowed")) {
		if(!vm.count("daemon")) {
			console_allowed = true;
		}
	}

	ConfigLoad(config);

	//Initialization log
	auto log_path = Config["log-path"].asString();

	if(vm.count("daemon")) {
		auto pid = fork();

		if(pid < 0) {
			LOG_E() << "Start daemon failed";
			return 1;
		} else if(pid == 0) { //Child

			umask(0);
			setsid();

			close(STDIN_FILENO);
			close(STDOUT_FILENO);
			close(STDERR_FILENO);

		} else { //parent
			return 0;
		}
	}

	ProgramLog::LogInit("main",
						log_path,
						console_allowed,
						getpid(),
						Config["clear-log"].asBool());

	//Start application
	LOG_D() << "Start application";
	MainApplication app;
	app.run();
	//}
	//catch(const boost::program_options::error &ec) {
	//	std::cout << std::endl << __PRETTY_FUNCTION__ << ":" << __FILE__ << ":" << __LINE__
	//			  << " Could't parse command line: " << ec.what() << std::endl;
	//	return 1;
	//}
	//catch(const std::runtime_error &err) {
	//	std::cout << std::endl << __PRETTY_FUNCTION__ << ":"
	//			  << __FILE__ << ":" << __LINE__ << ": " << err.what()
	//			  << std::endl;
	//	return 1;
	//}
	//catch(const std::exception &ex) {
	//	std::cout << std::endl << __PRETTY_FUNCTION__ << ":" << __FILE__ << ":"
	//			  << __LINE__ << ": " << ex.what() << std::endl;
	//	return 1;
	//}
	return 0;
}