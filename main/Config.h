//
// Created by bso on 10.12.17.
//

#ifndef UNITY_CONFIG_H
#define UNITY_CONFIG_H

#include <json.h>

namespace mainapp {

extern Json::Value Config;

void ConfigLoad(const std::string &config);

}

#endif //UNITY_CONFIG_H