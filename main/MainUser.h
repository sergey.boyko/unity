//
// Created by bso on 13.03.18.
//

#ifndef UNITY_MAINUSER_H
#define UNITY_MAINUSER_H

#include <peer/UserContext.h>
#include <boost/asio/deadline_timer.hpp>

namespace mainapp {

using namespace core;

class MainUser: public UserContext {
public:
	explicit MainUser(const upack::UserInfo &info);

	void onMessage(const std::string &data, size_t size, uint16_t pid, uint16_t type) final;

	std::shared_ptr<MainUser> shared_main_user();
};

}

#endif //UNITY_MAINUSER_H
