//
// Created by bso on 04.12.17.
//

#ifndef UNITY_AUTHENUMS_H
#define UNITY_AUTHENUMS_H

#define WaitAuthorizationTimeout 360

#define CookieSize 20

#define ClientNeedPing false

enum AuthFailedReason {
	UserNotFound = 1, //Authorization
	ServiceNotWorking,
	UserBanned,
	LoginIsBusy, //Registration
	IncorrectLoginOrPassword,
};

#endif //UNITY_AUTHENUMS_H
