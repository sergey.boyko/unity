//
// Created by bso on 29.10.17.
//

#ifndef UNITY_AUTHAPPLICATION_H
#define UNITY_AUTHAPPLICATION_H

#include <IApplication.h>
#include <upack.pb.h>
#include <ManyFacedListener.h>
#include <boost/asio/io_service.hpp>
#include <peer/UserContext.h>
#include "auth/AuthDb.h"
#include <unordered_map>

namespace mainapp {

using namespace core;

struct AuthUser;

class ClientPeersListener;

class RelayPeersListener;

class MainRelay;

class MainUser;

class MainApplication: public IApplication {
public:
	MainApplication();

	void run() final;

	void Restart() final;

	void Stop() final;

	static MainApplication *GetInstance() {
		return Instance;
	}

	void AddAuthClientPeer(const std::shared_ptr<AuthUser> &user);

	void RemoveAuthClientPeer(const std::shared_ptr<AuthUser> &user);

	void RemoveAuthClientPeer(uint32_t socket_id);

	template <class T>
	void handlePacket(const std::string &data);

	void OnRelayConnectRequest(const std::shared_ptr<ISocketChannel> &socket_channel,
							   const upack::RelayConnectRequest &fwd);

	AuthDb m_db;

	std::unordered_map<uint32_t, std::shared_ptr<MainRelay>> m_relay_peers;

private:
	static MainApplication *Instance;

	void processMessage(const upack::RelayFeedbackOnNewUser &fwd);

	void processMessage(const upack::ClientConnected &fwd);

	void processMessage(const upack::ClientDisconnected &fwd);

	struct ClientPeersListener: public ManyFacedListener {
	public:
		explicit ClientPeersListener(boost::asio::io_service &io);

		void onConnect(const std::shared_ptr<ISocketChannel> &socket_channel) final;

		void onMessage(const std::shared_ptr<ISocketChannel> &socket_channel,
					   const std::string &data,
					   size_t size,
					   uint16_t pid,
					   uint16_t type) final;

		void onLostConnection(const std::shared_ptr<ISocketChannel> &socket_channel) final;

		void onFinished(const std::shared_ptr<ISocketChannel> &socket_channel) final;
	};

	struct RelayPeersListener: public ManyFacedListener {
	public:
		explicit RelayPeersListener(boost::asio::io_service &io);

		void onConnect(const std::shared_ptr<ISocketChannel> &socket_channel) final;

		void onMessage(const std::shared_ptr<ISocketChannel> &socket_channel,
					   const std::string &data,
					   size_t size,
					   uint16_t pid,
					   uint16_t type) final;

		//void onLostConnection(const std::shared_ptr<ISocketChannel> &socket_channel) final;

		void onFinished(const std::shared_ptr<ISocketChannel> &socket_channel) final;
	};

	std::shared_ptr<ClientPeersListener> m_clients_listener;

	std::shared_ptr<RelayPeersListener> m_relays_listener;

	/**
	* first - socket_channel_id
	* second - UserContext
	*/
	std::unordered_map<uint32_t, std::shared_ptr<AuthUser>> m_user_peers_in_process_authorized;

	/**
	* first - m_uid
	* second - UserContext
	*/
	std::unordered_map<uint32_t, std::shared_ptr<MainUser>> m_user_peers;

	uint16_t m_relay_listen_other_relays_port_increment = 4000;
	uint16_t m_relay_listen_clients_port_increment = 5000;
};

#define App() MainApplication::GetInstance()

}

#endif //UNITY_AUTHAPPLICATION_H
