//
// Created by bso on 13.03.18.
//

#ifndef UNITY_MAINRELAY_H
#define UNITY_MAINRELAY_H

#include <memory>
#include <ISocketChannel.h>

namespace mainapp {

using namespace core;

class MainRelay {
public:
	/**
	* Ctor
	* @param socket_channel
	* @param listen_other_relays_port - on this port the relay will listen
	*/
	MainRelay(const std::shared_ptr<ISocketChannel> &socket_channel,
			  uint16_t listen_other_relays_port,
			  uint16_t listen_clients_port,
			  bool is_new);

	~MainRelay();

	std::shared_ptr<ISocketChannel> m_socket_channel;

	uint16_t m_listen_other_relays_port;
	uint16_t m_listen_clients_port;
	bool m_is_new;
};

}

#endif //UNITY_MAINRELAY_H
