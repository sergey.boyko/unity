//
// Created by bso on 29.10.17.
//

//#include <peer/UserContext.h>
#include <boost/asio/ip/tcp.hpp>
#include <peer/UserContext.h>
#include <google/protobuf/util/json_util.h>

#include "Config.h"
#include "MainApplication.h"
#include "auth/AuthUser.h"
#include "MainEnums.h"
#include "MainRelay.h"
#include "MainUser.h"

namespace mainapp {

using namespace core;
using namespace boost::asio::ip;

MainApplication *MainApplication::Instance = nullptr;

MainApplication::MainApplication():
		m_clients_listener(new ClientPeersListener(m_io)),
		m_db(Config["db"]["host"].asString(),
			 Config["db"]["login"].asString(),
			 Config["db"]["password"].asString(),
			 Config["db"]["schema"].asString()),
		m_relays_listener(new RelayPeersListener(m_io)) {
	LOG_D() << "MainApplication::MainApplication()";
	Instance = this;
}


void MainApplication::AddAuthClientPeer(const std::shared_ptr<AuthUser> &user) {
	m_user_peers_in_process_authorized[user->m_socket_channel->m_id] = user;
	user->run();
}


void MainApplication::RemoveAuthClientPeer(uint32_t socket_id) {
	m_user_peers_in_process_authorized.erase(socket_id);
}


void MainApplication::RemoveAuthClientPeer(const std::shared_ptr<AuthUser> &user) {
	m_user_peers_in_process_authorized.erase(user->m_socket_channel->m_id);
}


void MainApplication::run() {
	m_db.run();
	m_clients_listener->run();
	m_relays_listener->run();
	m_io.run();
}


void MainApplication::Restart() {
	//FIXME
}


void MainApplication::Stop() {
	//FIXME
}


template <class T>
void MainApplication::handlePacket(const std::string &data) {
	//TODO checking packet
	T fwd;
	if(fwd.ParseFromString(data)) {
		std::string json_output;
		google::protobuf::util::MessageToJsonString(fwd, &json_output);
		LOG_D() << "Received message: " << fwd.GetDescriptor()->full_name() << json_output;

		processMessage(fwd);
		return;
	}

	LOG_E() << "Couldn't parse packet";
}


void MainApplication::processMessage(const upack::RelayFeedbackOnNewUser &fwd) {
	LOG_D() << "Received RelayFeedbackOnNewUser uid = "
			<< fwd.m_uid()
			<< " result = "
			<< fwd.m_success();
	std::shared_ptr<AuthUser> user;
	for(const auto &peer : m_user_peers_in_process_authorized) {
		if(peer.second->m_uid == fwd.m_uid()) {
			user = peer.second;
		}
	}

	if(user) {
		user->onRelayMessage(fwd);
	} else {
		LOG_E() << "Couldn't find relay";
	}
}


void MainApplication::processMessage(const upack::ClientConnected &fwd) {
	LOG_I() << "Client uid = " << fwd.m_user().m_uid() << " connected to Relay";
	m_user_peers[fwd.m_user().m_uid()] = std::make_shared<MainUser>(fwd.m_user());
}


void MainApplication::processMessage(const upack::ClientDisconnected &fwd) {
	LOG_I() << "Client uid = " << fwd.m_uid() << " disconnected from Relay";
	m_user_peers.erase(fwd.m_uid());
}

void MainApplication::OnRelayConnectRequest(const std::shared_ptr<ISocketChannel> &socket_channel,
											const upack::RelayConnectRequest &fwd) {
	LOG_D() << "processMessage() RelayConnectRequest";
	upack::RelayConnectResponse packet;
	for(const auto &relay : App()->m_relay_peers) {
		auto it = packet.add_m_relay_list();
		auto listen_ip = relay.second->m_socket_channel->GetRemoteEndpoint().first;
		auto listen_port = relay.second->m_listen_other_relays_port;
		auto is_new = relay.second->m_is_new;
		it->set_m_ip(listen_ip);
		it->set_m_port(listen_port);
		it->set_m_is_new(is_new);
	}

	packet.set_m_listen_other_relays_port(App()->m_relay_listen_other_relays_port_increment);
	packet.set_m_listen_clients_port(App()->m_relay_listen_clients_port_increment);

	App()->m_relay_peers[socket_channel->m_id] =
			std::make_shared<MainRelay>(socket_channel,
										App()->m_relay_listen_other_relays_port_increment++,
										App()->m_relay_listen_clients_port_increment++,
										fwd.m_is_begins());

	socket_channel->SendPacket(upack::pRelayConnectResponse, packet);
}


MainApplication::ClientPeersListener::ClientPeersListener(boost::asio::io_service &io):
		ManyFacedListener(io,
						  tcp::endpoint(address::from_string(
								  Config["listeners"]["auth-clients"]["ip"].asString()),
										Config["listeners"]["auth-clients"]["port"].asUInt()),
						  ClientNeedPing) {
	LOG_D() << "MainApplication::ClientPeersListener::ClientPeersListener()";
}


void MainApplication::ClientPeersListener::onConnect(const std::shared_ptr<ISocketChannel> &socket_channel) {
	LOG_I() << "Connection established with AuthUser " <<
			socket_channel->GetRemoteEndpointString() <<
			" " <<
			socket_channel->GetStringId();
	std::shared_ptr<AuthUser> it(new AuthUser(App()->GetIO(), socket_channel,
											  socket_channel->m_id));
	App()->AddAuthClientPeer(it);
}


void MainApplication::ClientPeersListener::onMessage(const std::shared_ptr<ISocketChannel> &socket_channel,
													 const std::string &data,
													 size_t size,
													 uint16_t pid,
													 uint16_t type) {
	LOG_I() << "Received message from AuthUser "
			<< socket_channel->GetStringId()
			<< " "
			<< socket_channel->GetRemoteEndpointString()
			<< " pid = "
			<< pid;

	App()->m_user_peers_in_process_authorized[socket_channel->m_id]->onMessage(data,
																			   size,
																			   pid,
																			   type);
}


void MainApplication::ClientPeersListener::onLostConnection(const std::shared_ptr<ISocketChannel> &socket_channel) {
	ManyFacedListener::onLostConnection(socket_channel);
}


void MainApplication::ClientPeersListener::onFinished(const std::shared_ptr<ISocketChannel> &socket_channel) {
	LOG_I() << "AuthUser " << socket_channel->m_id << " lost connection";
	App()->RemoveAuthClientPeer(socket_channel->m_id);
	ManyFacedListener::onFinished(socket_channel);
}


MainApplication::RelayPeersListener::RelayPeersListener(boost::asio::io_service &io):
		ManyFacedListener(io,
						  tcp::endpoint(address::from_string(
								  Config["listeners"]["relays"]["ip"].asString()),
										Config["listeners"]["relays"]["port"].asUInt()),
						  ClientNeedPing) {
	LOG_D() << "ClientPeersListener()";
}


void MainApplication::RelayPeersListener::onConnect(
		const std::shared_ptr<ISocketChannel> &socket_channel) {
	LOG_I() << "Connection established with " <<
			socket_channel->GetRemoteEndpointString() <<
			" " <<
			socket_channel->GetStringId();
	//To do nothing, wait packet RelayConnectRequest
}


void MainApplication::RelayPeersListener::onMessage(
		const std::shared_ptr<ISocketChannel> &socket_channel,
		const std::string &data,
		size_t size,
		uint16_t pid,
		uint16_t type) {
	LOG_I() << "Received message from relay "
			<< socket_channel->GetStringId()
			<< " "
			<< socket_channel->GetRemoteEndpointString()
			<< " pid = "
			<< pid;

	switch(pid) {
		case upack::pRelayFeedbackOnNewUser: {
			App()->handlePacket<upack::RelayFeedbackOnNewUser>(data);
			break;
		}

		case upack::pClientConnected: {
			App()->handlePacket<upack::ClientConnected>(data);
			break;
		}

		case upack::pClientDisconnected: {
			App()->handlePacket<upack::ClientDisconnected>(data);
			break;
		}

		case upack::pRelayConnectRequest: {
			upack::RelayConnectRequest fwd;
			if(fwd.ParseFromString(data)) {
				App()->OnRelayConnectRequest(socket_channel, fwd);
			} else {
				LOG_E() << "Couldn't parse packet";
			}
			break;
		}

		default: {
			LOG_E() << "Received unknown packet";
			break;
		};
	}
}


void MainApplication::RelayPeersListener::onFinished(const std::shared_ptr<ISocketChannel> &socket_channel) {
	LOG_I() << "MainRelay id = " << socket_channel->m_id << " lost connection";

	App()->m_relay_peers.erase(socket_channel->m_id);
	ManyFacedListener::onFinished(socket_channel);
}


}