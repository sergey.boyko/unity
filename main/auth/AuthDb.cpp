//
// Created by bso on 22.11.17.
//

#include <ProgramLog.h>
#include "AuthDb.h"

namespace mainapp {

AuthDb::AuthDb(const std::string &host,
			   const std::string &login,
			   const std::string &password,
			   const std::string &schema):
		SingleThreadedSql(host, login, password, schema) {
	LOG_D() << "AuthDb()";
}

AuthDb::~AuthDb() {
	LOG_D() << "~AuthDb()";
}

}