//
// Created by bso on 24.11.17.
//

#include <boost/format.hpp>
#include <ProgramLog.h>
#include <db/ISqlRequestsListener.h>

#include "CheckExistenceSqlRequest.h"

namespace mainapp {

CheckExistenceSqlRequest::CheckExistenceSqlRequest(const std::shared_ptr<ISqlRequestsListener> &listener,
												   const std::string &username,
												   const std::string &password):
		ISqlRequest(listener),
		m_username(username) {
	m_request = "SELECT uid, isban, UNIX_TIMESTAMP(reg_date) FROM users WHERE username = '%s' AND password = '%s'";
	m_request = (boost::format(m_request) % username % password).str();
	LOG_D() << "CheckExistenceSqlRequest(username,password) request = "
			<< "\n"
			<< m_request;
}

CheckExistenceSqlRequest::CheckExistenceSqlRequest(
		const std::shared_ptr<ISqlRequestsListener> &listener,
		const std::string &username):
		ISqlRequest(listener),
		m_username(username) {
	m_request = "SELECT uid, isban, UNIX_TIMESTAMP(reg_date) FROM users WHERE username = '%s'";
	m_request = (boost::format(m_request) % username).str();
	LOG_D() << "CheckExistenceSqlRequest(username) request = "
			<< "\n"
			<< m_request;
}

CheckExistenceSqlRequest::~CheckExistenceSqlRequest() {
	LOG_D() << "~CheckExistenceSqlRequest()";
}

void CheckExistenceSqlRequest::OnResponse(sql::ResultSet *result) {
	int k = 0;
	while(result->next()) {
		m_response_uid = result->getUInt("uid");
		m_response_isban = result->getBoolean("isban");
		m_response_reg_data = result->getUInt64("UNIX_TIMESTAMP(reg_date)");
		++k;
	}

	if(k > 1) {
		LOG_E() << "In db existence " << k << " users with login = \""
				<< m_username << "\"";
	}

	m_listener->OnResponse(shared_from_this());
}

}