//
// Created by bso on 24.11.17.
//

#ifndef UNITY_CHECKEXISTENCESQLREQUEST_H
#define UNITY_CHECKEXISTENCESQLREQUEST_H

#include <db/ISqlRequest.h>

namespace mainapp {

using namespace core;

class CheckExistenceSqlRequest: public ISqlRequest {
public:
	CheckExistenceSqlRequest(const std::shared_ptr<ISqlRequestsListener> &listener,
									  const std::string &username,
									  const std::string &password);

	CheckExistenceSqlRequest(const std::shared_ptr<ISqlRequestsListener> &listener,
							 const std::string &username);

	~CheckExistenceSqlRequest();

	void OnResponse(sql::ResultSet *result) final;

	uint32_t m_response_uid = 0;

	std::time_t m_response_reg_data;

	bool m_response_isban;

	std::string m_username;
};

}

#endif //UNITY_CHECKEXISTENCESQLREQUEST_H
