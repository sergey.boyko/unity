//
// Created by bso on 09.03.18.
//

#ifndef UNITY_AuthUser11_H
#define UNITY_AuthUser11_H

#include <peer/UserContext.h>
#include <db/ISqlRequestsListener.h>
#include <IState.h>
#include <ISocketChannel.h>
#include "AuthDb.h"

namespace mainapp {

using namespace core;

class RegisterUserSqlRequest;

class CheckExistenceSqlRequest;

class MainRelay;

enum AuthClientStates {
	StateWaitAuthorization = 0,
	StateRegistrationStep1, //Check existence
	StateRegistrationStep2, //Register
	StateRegistrationStep3, //Receiving uid
	StateAuthorization,
	StateNotifyRelay,
	StateFinished
};

class AuthUser: public UserContext,
				public ISqlRequestsListener {
public:
	AuthUser(boost::asio::io_service &io,
			 const std::shared_ptr<ISocketChannel> &socket_channel,
			 uint64_t id);

	void run();

	void onMessage(const std::string &data, size_t size, uint16_t pid, uint16_t type) final;

	void onRelayMessage(const upack::RelayFeedbackOnNewUser &response);

	std::shared_ptr<AuthUser> shared_auth_user();

	void SetStateFinished();

	std::shared_ptr<ISocketChannel> m_socket_channel;

private:
	template <typename T>
	void handlePacket(const std::string &data);

	void processRequest(const upack::UserAuthRequest &fwd);

	void processRequest(const upack::UserRegRequest &fwd);

	void setStateWaitAuthorization();

	void setStateNotifyRelay();

	void OnResponse(const std::shared_ptr<ISqlRequest> &request) final;

	void OnFailed(const std::shared_ptr<ISqlRequest> &request) final;

	void sendAuthFailed(unsigned int result);

	boost::asio::deadline_timer m_timer;

	uint64_t m_id;
	unsigned int m_state;

	std::shared_ptr<RegisterUserSqlRequest> m_reg_request;
	std::shared_ptr<CheckExistenceSqlRequest> m_check_existence_request;

	std::shared_ptr<MainRelay> m_future_relay;

	std::random_device m_rd;
};

}

#endif //UNITY_AuthUser11_H
