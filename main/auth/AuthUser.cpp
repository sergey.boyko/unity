//
// Created by bso on 09.03.18.
//

#include <GlobalCommon.h>

#include "AuthUser.h"
#include "../MainEnums.h"
#include "../MainApplication.h"
#include "../MainRelay.h"

#include "CheckExistenceSqlRequest.h"
#include "RegisterUserSqlRequest.h"
#include "../Config.h"

namespace mainapp {

AuthUser::AuthUser(boost::asio::io_service &io,
				   const std::shared_ptr<ISocketChannel> &socket_channel,
				   uint64_t id):
		m_socket_channel(socket_channel),
		m_timer(io),
		m_id(id) {
	LOG_D() << "AuthUser::AuthUser() {" << m_id << "}";
	m_cookie = GetCookie(CookieSize);
}


void AuthUser::run() {
	LOG_D() << "AuthUser::run() {" << m_id << "}";

	setStateWaitAuthorization();
}


void AuthUser::onMessage(const std::string &data, size_t size, uint16_t pid, uint16_t type) {
	LOG_D() << "AuthUser::AuthUser::onMessage() {" << m_id << "} pid = "
			<< pid << " type = " << type;

	if(type != Protobuf) {
		//TODO
		SetStateFinished();
		return;
	}

	if(m_state == StateFinished) {
		return;
	}

	switch(pid) {
		case upack::pUserAuthRequest : {
			handlePacket<upack::UserAuthRequest>(data);
			return;
		}

		case upack::pUserRegRequest: {
			handlePacket<upack::UserRegRequest>(data);
			return;
		}

		default: {
			SetStateFinished();
			return;
		};
	}
}


std::shared_ptr<AuthUser> AuthUser::shared_auth_user() {
	return std::dynamic_pointer_cast<AuthUser>(UserContext::shared_from_this());
}


template <typename T>
void AuthUser::handlePacket(const std::string &data) {
	T fwd;
	if(fwd.ParseFromString(data)) {
		processRequest(fwd);
		return;
	}

	LOG_E() << "Couldn't parse packet";
}

void AuthUser::processRequest(const upack::UserAuthRequest &fwd) {
	if(m_state == StateWaitAuthorization) {
		m_state = StateAuthorization;

		LOG_D() << "Received Auth request {" << m_id << "}";
		m_username = fwd.m_login();
		m_password = fwd.m_password();
		//FIXME do not set m_password

		m_check_existence_request =
				std::make_shared<CheckExistenceSqlRequest>(shared_auth_user(),
														   fwd.m_login(),
														   fwd.m_password());

		App()->m_db.AddRequest(m_check_existence_request);
	} else {
		LOG_D() << "Receive auth request in other state: " << m_state;
	}
}


void AuthUser::processRequest(const upack::UserRegRequest &fwd) {
	if(m_state == StateWaitAuthorization) {
		m_state = StateRegistrationStep1;

		LOG_D() << "Received Reg request {" << m_id << "}";
		m_username = fwd.m_login();
		m_password = fwd.m_password();

		m_check_existence_request =
				std::make_shared<CheckExistenceSqlRequest>(shared_auth_user(),
														   m_username);

		App()->m_db.AddRequest(m_check_existence_request);
	} else {
		LOG_E() << "Receive Reg request in other state: " << m_state;
	}
}


void AuthUser::setStateWaitAuthorization() {
	LOG_D() << "AuthUser::setStateWaitAuthorization()";
	m_state = StateWaitAuthorization;

	m_timer.expires_from_now(boost::posix_time::seconds(WaitAuthorizationTimeout));
	m_timer.async_wait([=](const boost::system::error_code &ec) {
		if(ec) {
			return;
		}

		LOG_D() << "Was deadline {" << m_id << "}";
		SetStateFinished();
	});
}


void AuthUser::SetStateFinished() {
	LOG_D() << "AuthUser::SetStateFinished()";
	m_state = StateFinished;
	m_socket_channel->CloseConnecion();
}


void AuthUser::OnResponse(const std::shared_ptr<ISqlRequest> &request) {
	LOG_D() << "On sql response {" << m_id << "}";

	if(m_state == StateRegistrationStep1) {
		if(request == m_check_existence_request) {
			m_state = StateRegistrationStep2;

			auto it = std::dynamic_pointer_cast<CheckExistenceSqlRequest>(request);
			if(it->m_response_uid) {
				LOG_D() << "Register failed: Login if busy";
				sendAuthFailed(AuthFailedReason::LoginIsBusy);
				setStateWaitAuthorization();
				return;
			}

			LOG_D() << "Register: Success step 1 - check login in db";

			m_check_existence_request.reset();
			m_reg_request = std::make_shared<RegisterUserSqlRequest>(shared_auth_user(),
																	 m_username,
																	 m_password);
			App()->m_db.AddRequest(m_reg_request);
			return;
		}
	} else if(m_state == StateRegistrationStep2) {
		if(request == m_reg_request) {
			LOG_D() << "Register: Success step 2 - register in db";
			m_state = StateRegistrationStep3;
			m_reg_request.reset();
			m_check_existence_request =
					std::make_shared<CheckExistenceSqlRequest>(shared_auth_user(),
															   m_username);

			App()->m_db.AddRequest(m_check_existence_request);
			return;
		}
	} else if(m_state == StateRegistrationStep3 || m_state == StateAuthorization) {
		if(request == m_check_existence_request) {
			auto it = std::dynamic_pointer_cast<CheckExistenceSqlRequest>(request);
			if(!it->m_response_uid) {
				LOG_D() << "AuthFailedReason::UserNotFound";
				sendAuthFailed(AuthFailedReason::UserNotFound);
				setStateWaitAuthorization();
				return;
			}

			if(it->m_response_isban) {
				LOG_D() << "AuthFailedReason::UserBanned";
				sendAuthFailed(AuthFailedReason::UserBanned);
				setStateWaitAuthorization();
				return;
			}

			m_uid = it->m_response_uid;
			m_auth_timestamp = it->m_response_reg_data;
			m_check_existence_request.reset();

			setStateNotifyRelay();
		}
	}
}


void AuthUser::setStateNotifyRelay() {
	LOG_D() << "AuthUser::setStateNotifyRelay()";
	if(App()->m_relay_peers.empty()) {
		m_state = StateWaitAuthorization;
		upack::UserAuthResponse fwd;
		LOG_I() << "Auth failed. Relay list is empty " << m_uid;

		fwd.set_m_success(false);
		fwd.set_m_reason(AuthFailedReason::ServiceNotWorking);

		m_socket_channel->SendPacket(upack::pUserAuthResponse, fwd);

		return;
	}

	m_state = StateNotifyRelay;

	auto number = m_rd() % App()->m_relay_peers.size();
	auto j = 0;
	for(auto it : App()->m_relay_peers) {
		if(j++ == number) {
			m_future_relay = it.second;
		}
	}

	m_password.clear();
	upack::NotificationToRelayAboutNewUser fwd;
	FillMessage(fwd.mutable_m_user());
	m_future_relay->m_socket_channel->SendPacket(upack::pNotificationToRelayAboutNewUser, fwd);
}


void AuthUser::onRelayMessage(const upack::RelayFeedbackOnNewUser &response) {
	LOG_D() << "Receive message from Relay";
	if(m_state != StateNotifyRelay) {
		LOG_E() << "Received message from relay, but m_state != StateNotifyRelay";
		return;
	}

	if(response.m_success()) {
		upack::UserAuthResponse fwd;
		m_state = StateWaitAuthorization;

		auto relay_address = m_future_relay->m_socket_channel->GetRemoteEndpoint();
		fwd.set_m_success(true);
		fwd.set_m_cookie(m_cookie);
		fwd.set_m_relay_ip(relay_address.first);
		fwd.set_m_relay_port(m_future_relay->m_listen_clients_port);

		m_socket_channel->SendPacket(upack::pUserAuthResponse, fwd);
	} else {
		upack::UserAuthResponse fwd;
		m_state = StateWaitAuthorization;
		LOG_I() << "Auth failed. Relay couldn't accept m_uid = " << m_uid;

		fwd.set_m_success(false);
		fwd.set_m_reason(AuthFailedReason::ServiceNotWorking);

		m_socket_channel->SendPacket(upack::pUserAuthResponse, fwd);
	}
}


void AuthUser::OnFailed(const std::shared_ptr<ISqlRequest> &request) {
	LOG_D() << "Couldn't complete sql request";
	sendAuthFailed(AuthFailedReason::ServiceNotWorking);
	setStateWaitAuthorization();
}


void AuthUser::sendAuthFailed(unsigned int result) {
	upack::UserAuthResponse fwd;
	fwd.set_m_success(false);
	fwd.set_m_reason(result);
	m_socket_channel->SendPacket(upack::pUserAuthResponse, fwd);
}

}