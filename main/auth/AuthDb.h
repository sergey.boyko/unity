//
// Created by bso on 22.11.17.
//

#ifndef UNITY_AUTHDB_H
#define UNITY_AUTHDB_H

#include <db/SingleThreadedSql.h>

namespace mainapp {

using namespace core;

class AuthDb: public SingleThreadedSql {
public:
	AuthDb(const std::string &host,
		   const std::string &login,
		   const std::string &password,
		   const std::string &schema);

	~AuthDb();
};

}

#endif //UNITY_AUTHDB_H
