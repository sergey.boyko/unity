//
// Created by bso on 22.11.17.
//

#include <boost/format.hpp>
#include <ProgramLog.h>

#include "RegisterUserSqlRequest.h"
#include "db/ISqlRequestsListener.h"

namespace mainapp {

RegisterUserSqlRequest::RegisterUserSqlRequest(const std::shared_ptr<ISqlRequestsListener> &listener,
											   const std::string &username,
											   const std::string &password):
		ISqlRequest(listener) {
	m_request = "INSERT INTO users (username,password,reg_date) VALUES('%s','%s',now())";
	m_request = (boost::format(m_request) % username % password).str();
	LOG_D() << "RegisterUserSqlRequest() request = " << "\n" << m_request;
}

RegisterUserSqlRequest::~RegisterUserSqlRequest() {
	LOG_D() << "~RegisterUserSqlRequestquest()";
}

void RegisterUserSqlRequest::OnResponse(sql::ResultSet *result) {
	if(m_listener) {
		m_listener->OnResponse(shared_from_this());
	}
}

}