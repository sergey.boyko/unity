//
// Created by bso on 22.11.17.
//

#ifndef UNITY_CHECKINSQLREQUEST_H
#define UNITY_CHECKINSQLREQUEST_H

#include <db/ISqlRequest.h>

namespace mainapp {

using namespace core;

class RegisterUserSqlRequest: public ISqlRequest {
public:
	explicit RegisterUserSqlRequest(const std::shared_ptr<ISqlRequestsListener> &listener,
									const std::string &username,
									const std::string &password);

	~RegisterUserSqlRequest();

	void OnResponse(sql::ResultSet *result) final;
};

}

#endif //UNITY_CHECKINSQLREQUEST_H
