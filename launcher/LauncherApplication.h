//
// Created by bso on 28.10.17.
//

#ifndef UNITY_LAUNCHER_H
#define UNITY_LAUNCHER_H

#include <boost/asio/signal_set.hpp>
#include <boost/asio/posix/stream_descriptor.hpp>

#include <IApplication.h>

namespace launcher {

using namespace boost::asio;
using namespace core;

class LauncherApplication: public IApplication {
public:
	explicit LauncherApplication(const std::string &config);

	~LauncherApplication();

	void run() final;

	void Restart() final;

	void Stop() final;

protected:

	bool callExecv();

	void onChildFinished();

	void startApplication();

	pid_t m_child_pid;

	std::array<char, 4096> m_buffer;

	posix::stream_descriptor m_child_process_listener;

	bool m_stop:1;

	std::string m_config;
};

}

#endif //UNITY_LAUNCHER_H
