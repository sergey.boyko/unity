//
// Created by bso on 28.10.17.
//

#include <unistd.h>
#include <boost/asio/buffer.hpp>
#include <ProgramLog.h>
#include "Config.h"

#include "LauncherApplication.h"


namespace launcher {

LauncherApplication::LauncherApplication(const std::string &config):
		m_child_process_listener(m_io),
		m_stop(false),
		m_config(config) {
	struct sigaction sa;
	std::memset(&sa, 0, sizeof(sa));
	sa.sa_handler = SIG_IGN;
	sigaction(SIGCHLD, &sa, nullptr);
}


LauncherApplication::~LauncherApplication() {
	if(m_child_process_listener.is_open()) {
		m_child_process_listener.close();
	}

	if(!m_io.stopped()) {
		m_io.stop();
	}

}


void LauncherApplication::run() {
	startApplication();

	m_io.run();
}


void LauncherApplication::Restart() {
	if(m_child_pid > 0) {
		kill(m_child_pid, SIGUSR1);
	}
}


void LauncherApplication::Stop() {
	m_stop = true;
	if(m_child_pid > 0) {
		kill(m_child_pid, SIGUSR1);
	}
}


bool LauncherApplication::callExecv() {
	//Init UNIX pipe
	//pipe[0] - open parent file descriptor
	//pipe[1] - open child file descriptor
	int fds[2];
	if(pipe(fds) < 0) {
		LOG_E() << "Couldn't create pipe";
		return false;
	}
	if(m_child_process_listener.is_open()) {
		m_child_process_listener.close();
	}
	m_child_process_listener.assign(fds[0]);

	m_io.notify_fork(io_service::fork_prepare);
	m_child_pid = fork();

	if(m_child_pid < 0) { //fork failed
		LOG_E() << "Fork failed";
		return false;
	} else if(m_child_pid == 0) { //Child context
		ProgramLog::LogInit("launcher", Config["log-path"].asString(), false, getpid(), Config["clear-log"].asBool(),
							"worker", false);
		m_io.notify_fork(io_service::fork_child);

		close(fds[0]);
		std::vector<char *> argv;
		argv.push_back(strdup(Config["exe"].asCString()));
		if(Config["argv"].isArray()) {
			if(Config["argv"].size() > 0) {
				for(const auto &it : Config["argv"]) {
					argv.push_back(strdup(it.asCString()));
				}
			}
		}
		argv.push_back(nullptr);

		LOG_D() << "Start application: " << argv[0];
		for(auto i = 1; i < argv.size() - 1; ++i) {
			LOG_D() << argv[i];
		}

		if(!execv(argv[0], argv.data())) {
			LOG_E() << "Couldn't launch \"" << Config["exe"].asCString() << "\"";
			exit(1);
		}
	} else { //Parent context
		m_io.notify_fork(io_service::fork_parent);
		close(fds[1]);

		m_child_process_listener.async_read_some(
				buffer(m_buffer), [=](const boost::system::error_code &ec, size_t) {
					if(ec) {
						onChildFinished();
					}
				});
	}
	LOG_D() << "Child started, id = " << m_child_pid;

	return true;
}


void LauncherApplication::onChildFinished() {
	if(m_child_process_listener.is_open()) {
		m_child_process_listener.close();
	}

	if(!m_stop) {
		startApplication();
		return;
	}

	m_io.stop();
}

void LauncherApplication::startApplication() {
	ConfigLoad(m_config);

	if(!callExecv()) {
		m_io.stop();
	}
}

}