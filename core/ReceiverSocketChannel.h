//
// Created by bso on 22.10.17.
//

#ifndef UNITY_RECEIVERSOCKETCHANNEL_H
#define UNITY_RECEIVERSOCKETCHANNEL_H

#include "ISocketChannel.h"

namespace core {

class ReceiverSocketChannel: public ISocketChannel {
public:
	ReceiverSocketChannel(io_service &io,
						  ISocketListener *listener,
						  const tcp::endpoint &ep,
						  uint32_t channel_id,
						  bool need_ping = false): ISocketChannel(io, listener, ep, channel_id, need_ping) {
		LOG_D() << "ReceiverSocketChannel()";
	}

	~ReceiverSocketChannel() final{
		LOG_D() << "~ReceiverSocketChannel()";
	}

	void run() final;

	void onConnect();

	tcp::socket &GetSocket();

	//protected:
	struct StateWaitConnection: public IState<ISocketChannel> {
		StateWaitConnection(): IState("StateWaitConnection") {
		}

		void onChanged() final;

		bool checkStateAllowed(const std::string &new_state_name) final;
	};

};

}


#endif //UNITY_RECEIVERSOCKETCHANNEL_H
