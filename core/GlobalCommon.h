//
// Created by bso on 10.03.18.
//

#ifndef UNITY_GLOBALCOMMON_H
#define UNITY_GLOBALCOMMON_H

#include <string>

namespace core {

std::string GetCookie(std::size_t size);

}


#endif //UNITY_GLOBALCOMMON_H
