//
// Created by bso on 22.10.17.
//

#include "ReceiverSocketChannel.h"

namespace core {

void ReceiverSocketChannel::StateWaitConnection::onChanged() {
	//m_ctx->m_socket->open(m_ctx->m_ep.protocol());
}


bool ReceiverSocketChannel::StateWaitConnection::checkStateAllowed(const std::string &new_state_name) {
	if(new_state_name == "StateLostConnection" || new_state_name == "StateWork" ||
	   new_state_name == "StateDone") {
		return true;
	}
	LOG_E() << "Incorrect change the state from " << m_state_name << " to " << new_state_name;
	m_ctx->setState<StateDone>();
	return false;
}


void ReceiverSocketChannel::run() {
	LOG_D() << "Start wait peers on: " << m_ep.address().to_string() << ":" << m_ep.port();
	setState<StateWaitConnection>();
}


void ReceiverSocketChannel::onConnect() {
	setState<StateWork>(m_socket->get_io_service());
}


tcp::socket &ReceiverSocketChannel::GetSocket() {
	return *m_socket;
}

}
