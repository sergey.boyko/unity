//
// Created by bso on 09.03.18.
//

#ifndef UNITY_VIRTUAL_ENABLE_SHARED_FROM_THIS_H
#define UNITY_VIRTUAL_ENABLE_SHARED_FROM_THIS_H

#include <memory>

struct virtual_enable_shared_from_this_base:
		std::enable_shared_from_this<virtual_enable_shared_from_this_base> {
	virtual ~virtual_enable_shared_from_this_base();
};
template<typename T>
struct virtual_enable_shared_from_this:
		virtual virtual_enable_shared_from_this_base {
	std::shared_ptr<T> shared_from_this() {
		return std::dynamic_pointer_cast<T>(
				virtual_enable_shared_from_this_base::shared_from_this());
	}
};

#endif //UNITY_VIRTUAL_ENABLE_SHARED_FROM_THIS_H
