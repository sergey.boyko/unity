//
// Created by bso on 05.10.17.
//

#include "ISocketChannel.h"
#include "ISocketListener.h"
#include <google/protobuf/util/json_util.h>
#include <upack.pb.h>

namespace core {

#define PING_INTERVAL 10


void ISocketChannel::SendPacket(uint16_t pid, const google::protobuf::Message &message) {
	std::string data = message.SerializeAsString();
	std::string ready_made_packet = m_message_parser.InsertHeader(data, pid);

	std::string json_output;
	google::protobuf::util::MessageToJsonString(message, &json_output);
	LOG_D() << "Send message: " << message.GetDescriptor()->full_name() << json_output;

	SendMessage(ready_made_packet);
}


void ISocketChannel::SendMessage(const std::string &message) {
	auto header = m_message_parser.ParseMessage(message.data());
	if(header.m_type != Protobuf) {
		LOG_D() << "Send message: " << m_message_parser.EraseHeader(message);
	}
	m_write_buffer.push(message);

	if(m_write_buffer.size() == 1) {
		if(m_state) {
			if(m_state->m_state_name == "StateWork") {
				//FIXME may don't work
				std::dynamic_pointer_cast<StateWork>(m_state)->continueWrite();
			}
		}
	}
}


bool ISocketChannel::Connected() {
	if(m_state) {
		if(m_state->m_state_name == "StateWork") {
			return true;
		}
	}
	return false;
}


void ISocketChannel::CloseConnecion() {
	setState<StateDone>();
}


io_service &ISocketChannel::GetIoService() {
	return m_socket->get_io_service();
}

std::string ISocketChannel::GetRemoteEndpointString() {
	return m_socket->remote_endpoint().address().to_string() + ":" +
		   std::to_string(m_socket->remote_endpoint().port());
}

std::string ISocketChannel::GetStringId() {
	return "{" + std::to_string(m_id) + "}";
}

std::pair<std::string, uint16_t> ISocketChannel::GetRemoteEndpoint() {
	return std::make_pair(m_socket->remote_endpoint().address().to_string(),
						  m_socket->remote_endpoint().port());
}


void ISocketChannel::StateWork::onChanged() {
	continueRead();

	if(!m_ctx->m_write_buffer.empty()) {
		continueWrite();
	}

	if(m_ctx->m_need_ping) {
		continueSendPing();
	}

	m_ctx->m_listener->onConnect(m_ctx);
}


void ISocketChannel::StateWork::continueRead() {
	m_ctx->m_socket->async_receive(
			boost::asio::buffer(m_ctx->m_read_buffer),
			[=](const boost::system::error_code &ec, size_t size) {
				if(ec) {
					if(ec == boost::asio::error::operation_aborted) {
						return;
					}
					m_ctx->setState<StateLostsConnection>();
					return;
				}
				if(!m_ctx->m_message_parser.CheckReceivedPacket(
						m_ctx->m_read_buffer.data(), size)) {
					LOG_E() << "Received incomplete message";
					continueRead();
					return;
				}
				std::string data(m_ctx->m_read_buffer.begin(),
								 m_ctx->m_read_buffer.end());

				//Clear read buffer before continueRead
				m_ctx->m_read_buffer.fill(0);
				continueRead();

				m_ping_response = isPong(data);
				if(m_ping_response) {
					LOG_D() << "Received Pong";
					return;
				}

				//If data is Ping, function isPing will send Pong
				if(isPing(data)) {
					LOG_D() << "Received Ping";
					return;
				}

				//Get message id
				size_t size_output;
				uint16_t pid;
				uint16_t type_message;
				auto new_data = m_ctx->m_message_parser.EraseHeader(data,
																	size_output,
																	pid,
																	type_message);
				m_ctx->m_listener->onMessage(m_ctx, new_data, size, pid, type_message);
			});
}


void ISocketChannel::StateWork::continueWrite() {
	if(m_ctx->m_write_buffer.empty()) {
		return;
	}

	m_ctx->m_socket->async_send(boost::asio::buffer(m_ctx->m_write_buffer.front()),
								[=](const boost::system::error_code &ec, size_t size) {
									if(ec) {
										if(ec == boost::asio::error::operation_aborted) {
											return;
										}
										m_ctx->setState<StateLostsConnection>();
										return;
									}
									m_ctx->m_write_buffer.pop();
									//FIXME continueRead();
									continueWrite();
								});
}


void ISocketChannel::StateWork::continueSendPing() {
	m_ping_timer.expires_from_now(boost::posix_time::seconds(PING_INTERVAL));
	m_ping_timer.async_wait([=](const boost::system::error_code &ec) {
		if(ec) {
			return;
		}
		if(!m_ping_response) {
			m_ctx->setState<StateLostsConnection>();
			return;
		}
		upack::Ping fwd;
		m_magic_number = rd();
		fwd.set_m_magic_number(m_magic_number);
		m_ctx->SendPacket(upack::pPing, fwd);
		m_ping_response = false;
		continueSendPing();
	});
}


bool ISocketChannel::StateWork::checkStateAllowed(const std::string &new_state_name) {
	if(new_state_name == "StateLostConnection" || new_state_name == "StateDone") {
		return true;
	}
	LOG_E() << "Incorrect change the state from " << m_state_name << " to " << new_state_name;
	m_ctx->setState<StateDone>();
	return false;
}

bool ISocketChannel::StateWork::isPong(const std::string &data) {
	uint16_t pid, type;
	size_t size_output;
	auto new_data = m_ctx->m_message_parser.EraseHeader(data, size_output, pid, type);
	if(type != Protobuf) {
		return false;
	}

	if(pid != upack::pPong) {
		return false;
	}

	upack::Pong message;
	message.ParseFromString(new_data);
	return message.m_magic_number() == m_magic_number;

}

bool ISocketChannel::StateWork::isPing(const std::string &data) {
	uint16_t pid, type;
	size_t size_output;
	auto new_data = m_ctx->m_message_parser.EraseHeader(data, size_output, pid, type);
	if(type != Protobuf) {
		return false;
	}

	if(pid != upack::pPing) {
		return false;
	}

	upack::Ping message;
	message.ParseFromString(new_data);

	upack::Pong fwd;
	fwd.set_m_magic_number(message.m_magic_number());
	m_ctx->SendPacket(upack::pPong, fwd);
	return true;
}


void ISocketChannel::StateLostsConnection::onChanged() {
	boost::system::error_code ec;
	if(m_ctx->m_socket->is_open()) {
		m_ctx->m_socket->close(ec);
	}
	m_ctx->m_listener->onLostConnection(m_ctx);
}


bool ISocketChannel::StateLostsConnection::checkStateAllowed(const std::string &new_state_name) {
	if(new_state_name == "StateConnect" || new_state_name == "StateWaitConnection" || new_state_name == "StateDone") {
		return true;
	}
	LOG_E() << "Incorrect change the state from " << m_state_name << " to " << new_state_name;
	m_ctx->setState<StateDone>();
	return false;
}


void ISocketChannel::StateDone::onChanged() {
	boost::system::error_code ec;
	if(m_ctx->m_socket->is_open()) {
		m_ctx->m_socket->close(ec);
	}
	m_ctx->m_listener->onFinished(m_ctx);
	m_ctx.reset();
}


}
