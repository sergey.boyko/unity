//
// Created by bso on 04.03.18.
//

#include <google/protobuf/util/json_util.h>
#include "PosixLocalSocket.h"
#include "ProgramLog.h"

namespace core {

PosixLocalSocket::PosixLocalSocket(IPosixLocalSocketListener *listener,
								   boost::asio::io_service &io,
								   boost::asio::local::stream_protocol::socket &socket):
		m_listener(listener),
		m_io(io),
		m_local_socket(std::move(socket)) {
}

void PosixLocalSocket::run() {
	LOG_D() << "Start read posix socket";
	continueRead();
}

void PosixLocalSocket::continueRead() {
	m_local_socket.async_receive(
			boost::asio::buffer(m_read_buffer),
			[=](const boost::system::error_code &ec, size_t size) {
				if(ec) {
					m_listener->onFinished(shared_from_this());
					return;
				}
				if(!m_message_parser.CheckReceivedPacket(
						m_read_buffer.data(), size)) {
					LOG_E() << "Received incomplete message";
					continueRead();
					return;
				}

				continueRead();
				std::string data(m_read_buffer.begin(),
								 m_read_buffer.end());

				//Get message id
				size_t size_output;
				uint16_t pid;
				uint16_t type_message;
				auto new_data = m_message_parser.EraseHeader(data,
															 size_output,
															 pid,
															 type_message);
				m_listener->onMessage(shared_from_this(),
									  new_data,
									  size,
									  pid,
									  type_message);
			});
}

void PosixLocalSocket::continueWrite() {
	if(m_write_buffer.empty()) {
		return;
	}

	m_local_socket.async_send(
			boost::asio::buffer(m_write_buffer.front()),
			[=](const boost::system::error_code &ec, size_t size) {
				if(ec) {
					if(ec == boost::asio::error::operation_aborted) {
						return;
					}
					m_listener->onFinished(shared_from_this());
					return;
				}
				m_write_buffer.pop();
				continueWrite();
			});
}

void PosixLocalSocket::SendPacket(uint16_t pid, const google::protobuf::Message &message) {
	std::string data = message.SerializeAsString();
	std::string ready_made_packet = m_message_parser.InsertHeader(data, pid);

	std::string json_output;
	google::protobuf::util::MessageToJsonString(message, &json_output);
	LOG_D() << "Send message: "
			<< message.GetDescriptor()->full_name()
			<< "\n"
			<< json_output;

	SendMessage(ready_made_packet);
}

void PosixLocalSocket::SendMessage(const std::string &message) {
	auto header = m_message_parser.ParseMessage(message.data());
	if(header.m_type != Protobuf) {
		LOG_D() << "Send message: " << m_message_parser.EraseHeader(message);
	}
	m_write_buffer.push(message);

	if(m_write_buffer.size() == 1) {
		continueWrite();
	}
}

}