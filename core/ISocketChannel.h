//
// Created by bso on 05.10.17.
//

#ifndef ALPMAIN_ISOCKETLISTNER_H
#define ALPMAIN_ISOCKETLISTNER_H

#include "IState.h"
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <queue>
#include "ProtoParser.h"
#include <google/protobuf/message.h>

using boost::asio::ip::tcp;
using boost::asio::io_service;

namespace core {

#define PING_INTERVAL 6
#define MAX_RECEIVE 4096


class ISocketListener;

class ISocketChannel: public IStateMachine<ISocketChannel> {
public:
	ISocketChannel(io_service &io,
				   ISocketListener *listener,
				   const tcp::endpoint &ep,
				   uint32_t channel_id,
				   bool need_ping):
			IStateMachine(),
			m_id(channel_id),
			m_socket(new tcp::socket(io)),
			m_ep(ep),
			m_listener(listener),
			m_need_ping(need_ping) {
	}

	virtual ~ISocketChannel(){
		LOG_D() << "~ISocketChannel()";
	}

	void SendPacket(uint16_t pid, const google::protobuf::Message &message);

	void SendMessage(const std::string &message);

	virtual void run() = 0;

	std::string GetRemoteEndpointString();

	std::pair<std::string, uint16_t> GetRemoteEndpoint();

	bool Connected();

	void CloseConnecion();

	io_service &GetIoService();

	std::string GetStringId();

	uint32_t m_id;

//protected:

	struct StateDone: public IState<ISocketChannel> {
		/**
		* Ctor
		*/
		StateDone(): IState("StateDone") {
		}

		void onChanged() final;

		bool checkStateAllowed(const std::string &new_state_name) final {
			return false;
		}
	};

	struct StateLostsConnection: public IState<ISocketChannel> {
		StateLostsConnection(): IState("StateLostConnection") {
		}

		void onChanged() final;

		bool checkStateAllowed(const std::string &new_state_name) final;
	};

	struct StateWork: public IState<ISocketChannel> {
		explicit StateWork(io_service &io):
				IState("StateWork"),
				m_ping_timer(io) {
		}

		void onChanged() final;

		bool checkStateAllowed(const std::string &new_state_name) final;

		void continueRead();

		void continueWrite();

		void continueSendPing();

		bool isPong(const std::string &data);

		bool isPing(const std::string &data);

		boost::asio::deadline_timer m_ping_timer;

		std::random_device rd;

		/**
		* True, if received Pong, and magic number was right
		*/
		bool m_ping_response = true;

		uint32_t m_magic_number;
	};

	std::shared_ptr<tcp::socket> m_socket;

	tcp::endpoint m_ep;

	std::array<char, MAX_RECEIVE> m_read_buffer;

	std::queue<std::string> m_write_buffer;

	ISocketListener *m_listener;

	ProtoParser m_message_parser;

	bool m_need_ping;

};

}

#endif //ALPMAIN_ISOCKETLISTNER_H
