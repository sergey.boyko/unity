//
// Created by bso on 22.10.17.
//

#ifndef UNITY_CALLERSOCKETCHANNEL_H
#define UNITY_CALLERSOCKETCHANNEL_H

#include <ISocketChannel.h>

namespace core {

class CallerSocketChannel: public ISocketChannel {
public:
	CallerSocketChannel(io_service &io,
						  ISocketListener *listener,
						  const tcp::endpoint &ep,
						  uint32_t channel_id,
						  bool need_ping = false): ISocketChannel(io, listener, ep, channel_id, need_ping) {
		LOG_D() << "CallerSocketChannel()";
	}

	~CallerSocketChannel(){
		LOG_D() << "~CallerSocketChannel()";
	}

	void run() final;

//protected:
	/**
	 * State connection attempt
	 */
	struct StateConnect: public IState<ISocketChannel> {
		/**
		 * Ctor
		 */
		StateConnect(): IState("StateConnect") {
		}

		void onChanged() final;

		bool checkStateAllowed(const std::string &new_state_name) final;
	};

};

}

#endif //UNITY_CALLERSOCKETCHANNEL_H
