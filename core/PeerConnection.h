//
// Created by bso on 23.10.17.
//

#ifndef UNITY_PEERCONNECT_H
#define UNITY_PEERCONNECT_H

#include "ISocketListener.h"
#include "ProgramLog.h"
#include <boost/asio.hpp>
#include <google/protobuf/message.h>

namespace core {

#define BAD_ACCEPT_TIMEOUT 1

using boost::asio::io_service;
using boost::asio::ip::tcp;

class ISocketChannel;

class CallerSocketChannel;

class PeerConnection: public ISocketListener {
public:
	PeerConnection(boost::asio::io_service &io,
				   const tcp::endpoint &ep,
				   bool need_ping);

	PeerConnection(boost::asio::io_service &io, bool need_ping);

	~PeerConnection() {
		m_reconnect_timer.cancel();
		m_peer.reset();
		LOG_D() << "~PeerConnection()";
	}

	void SendPacket(uint16_t pid, const google::protobuf::Message &message);

	virtual void run();

	virtual void onConnect(const std::shared_ptr<ISocketChannel> &socket_channel);

	virtual void onMessage(const std::shared_ptr<ISocketChannel> &socket_channel, const std::string &data, size_t size,
						   uint16_t pid, uint16_t type);

	virtual void onLostConnection(const std::shared_ptr<ISocketChannel> &socket_channel);

	virtual void onFinished(const std::shared_ptr<ISocketChannel> &socket_channel);

	void CloseConnection();

protected:
	std::shared_ptr<CallerSocketChannel> m_peer;

	tcp::endpoint m_remote_ep;

	boost::asio::deadline_timer m_reconnect_timer;

	bool m_need_ping;
};

}

#endif //UNITY_PEERCONNECT_H
