//
// Created by bso on 10.03.18.
//

#include <random>

#include "GlobalCommon.h"

namespace core {

std::random_device rd;

char GetRandomChar() {
	auto ch = static_cast<char>(rd() % 122);
	if((ch >= 48 && ch <= 57)
	   || (ch >= 65 && ch <= 90)
	   || (ch >= 97 && ch <= 122)) {
		return ch;
	} else {
		return GetRandomChar();
	}
}

std::string GetCookie(std::size_t size) {
	std::string cookie;
	for(auto i = 0; i < size; ++i) {
		cookie.push_back(GetRandomChar());
	}

	return cookie;
}

}