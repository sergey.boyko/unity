//
// Created by bso on 21.10.17.
//

#ifndef UNITY_MANYFACEDPEER_H
#define UNITY_MANYFACEDPEER_H

#include "ISocketListener.h"
#include "ProgramLog.h"

#include <boost/asio/io_service.hpp>
#include <unordered_map>
#include <memory>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/deadline_timer.hpp>

namespace core {

#define BAD_ACCEPT_TIMEOUT_MS 10

using boost::asio::io_service;
using boost::asio::ip::tcp;

class ISocketChannel;

class ReceiverSocketChannel;


class ManyFacedListener: public ISocketListener {
public:
	explicit ManyFacedListener(boost::asio::io_service &io,
							   const tcp::endpoint &ep,
							   bool need_ping);

	~ManyFacedListener();

	void run();

	virtual void onConnect(const std::shared_ptr<ISocketChannel> &socket_channel) override;

	virtual void onMessage(const std::shared_ptr<ISocketChannel> &socket_channel,
						   const std::string &data,
						   size_t size,
						   uint16_t pid,
						   uint16_t type) override;

	virtual void onLostConnection(const std::shared_ptr<ISocketChannel> &socket_channel) override;

	virtual void onFinished(const std::shared_ptr<ISocketChannel> &socket_channel) override;

	void SetListenEndpoint(const tcp::endpoint &ep);

protected:
	void continueAccept();

	boost::asio::io_service &m_io;

	boost::asio::ip::tcp::acceptor m_acceptor;

	std::unordered_map<uint32_t, std::shared_ptr<ReceiverSocketChannel>> m_peers;

	tcp::endpoint m_local_ep;

	std::shared_ptr<ReceiverSocketChannel> m_new_peer;

	uint32_t m_peer_increment = 0;

	bool m_need_ping;
};

}

#endif //UNITY_MANYFACEDPEER_H
