//
// Created by bso on 21.10.17.
//

#include "IApplication.h"
#include "ProgramLog.h"

namespace core {

void IApplication::handleSigusr1(const boost::system::error_code &err, int signal) {
	if(signal == SIGUSR1) {
		LOG_D() << "Received SIGUSR1";
		Restart();
	}
}

void IApplication::handleSigusr2(const boost::system::error_code &err, int signal) {
	if(signal == SIGUSR2) {
		LOG_D() << "Received SIGUSR2";
		Stop();
	}
}

io_service &IApplication::GetIO() {
	return m_io;
}

}