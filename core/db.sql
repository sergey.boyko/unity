create table users(
uid int not null unique AUTO_INCREMENT primary key,
username varchar(20) not null unique,
password varchar(32) not null,
isban boolean default FALSE,
reg_date timestamp default CURRENT_TIMESTAMP,
first_name varchar(20) not null default '',
second_name varchar(20) not null default '',
photo_url varchar(120) not null default '',
language varchar(20) not null default 'eng',
client_type varchar(20) not null default 'base'
);