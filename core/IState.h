//
// Created by bso on 06.10.17.
//

#ifndef ALPMAIN_ISTATE_H
#define ALPMAIN_ISTATE_H

#include <string>
#include <memory>
#include "ProgramLog.h"

namespace core {

template <typename CTX>
class IStateMachine;

template <typename CTX>
struct IState {
	/**
	* Ctor
	* @param state_name - State name
	*/
	explicit IState(const std::string &state_name): m_state_name(state_name) {

	};

	/**
	 * On changed state
	 */
	virtual void onChanged() = 0;

	virtual bool checkStateAllowed(const std::string &new_state_name)=0;

	/**
	 * State name
	 */
	std::string m_state_name;

	/**
	 * Context of inheritor from the IStateMachine
	 */
	std::shared_ptr<CTX> m_ctx;
};


template <typename CTX>
class IStateMachine: public std::enable_shared_from_this<CTX> {
public:
	//~IStateMachine() = default;

	//FIXME Couldn't been protected
	//protected:
	/**
	 * Set state S
	 * @tparam S - New state
	 * @tparam T - State that changed to S
	 */
	template <typename S, typename ... Types>
	bool setState(Types &&... args) {
		auto newState = new S(args ...);
		if(!beforeChangingState(newState)) {
			//	m_state.reset();
			return false;
		}

		//change state
		newState->m_ctx = this->shared_from_this();
		m_state.reset(newState);
		m_state->onChanged();
		return true;
	};

	/**
	* Actions befor changing state
	* @tparam S - Type of new state
	* @param newState - New state
	* @return - True, if all right
	*/
	template <typename S>
	bool beforeChangingState(S *newState) {
		if(!m_state) {
			LOG_D() << "Change state from Null to "
					<< newState->m_state_name;
			return true;
		}

		if(!m_state->checkStateAllowed(newState->m_state_name)) {
			return false;
		}
		LOG_D() << "Change state from " << m_state->m_state_name << " to "
				<< newState->m_state_name;
		return true;
	}

	/**
	* State
	*/
	std::shared_ptr<IState<CTX>> m_state;

};

}
#endif //ALPMAIN_ISTATE_H
