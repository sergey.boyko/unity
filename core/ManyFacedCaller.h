//
// Created by bso on 12.03.18.
//

#ifndef UNITY_MANYFACEDCALLER_H
#define UNITY_MANYFACEDCALLER_H

#include "ISocketListener.h"
#include "ProgramLog.h"

#include <boost/asio/io_service.hpp>
#include <unordered_map>
#include <memory>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/deadline_timer.hpp>

namespace core {

#define BAD_ACCEPT_TIMEOUT 2

using boost::asio::io_service;
using boost::asio::ip::tcp;

class ISocketChannel;

class CallerSocketChannel;

class ManyFacedCaller: ISocketListener {
public:
	explicit ManyFacedCaller(boost::asio::io_service &io, const std::vector<tcp::endpoint> addresses);

	~ManyFacedCaller() {
		LOG_D() << "~ManyFacedCaller";
		m_timer.cancel();

		for(auto &peer : m_peers) {
			ClosePeer(peer.first);
		}
	}

	virtual void run();

	void ClosePeer(uint32_t id);

	void ClosePeer(std::shared_ptr<ISocketChannel> peer);

	void AddPeer(const tcp::endpoint &ep);

	void Stop();

	virtual void onConnect(const std::shared_ptr<ISocketChannel> &socket_channel) override;

	virtual void onMessage(const std::shared_ptr<ISocketChannel> &socket_channel,
						   const std::string &data,
						   size_t size,
						   uint16_t pid,
						   uint16_t type) override;

	virtual void onLostConnection(const std::shared_ptr<ISocketChannel> &socket_channel) override;

	virtual void onFinished(const std::shared_ptr<ISocketChannel> &socket_channel) override;

protected:
	boost::asio::io_service &m_io;

	boost::asio::deadline_timer m_timer;

	std::unordered_map<uint32_t, std::shared_ptr<CallerSocketChannel>> m_peers;

	tcp::endpoint m_local_ep;

	uint32_t m_peer_increment = 0;

	bool m_need_ping;
};

}

#endif //UNITY_MANYFACEDCALLER_H
