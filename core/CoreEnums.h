//
// Created by bso on 03.12.17.
//

#ifndef UNITY_COREENUMS_H
#define UNITY_COREENUMS_H

namespace core {

enum MessageType {
	Protobuf = 1,
	Json = 2,
	String = 3
};

}

#endif //UNITY_COREENUMS_H
