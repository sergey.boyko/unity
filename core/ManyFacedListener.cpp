//
// Created by bso on 21.10.17.
//

#include "ReceiverSocketChannel.h"
#include "ManyFacedListener.h"

namespace core {

ManyFacedListener::ManyFacedListener(boost::asio::io_service &io, const tcp::endpoint &ep, bool need_ping):
		m_io(io),
		m_acceptor(io, ep),
		m_local_ep(ep),
		m_need_ping(need_ping) {
	LOG_D() << "ManyFacedListener()";
	m_new_peer.reset(new ReceiverSocketChannel(io,
											   this,
											   m_local_ep,
											   m_peer_increment++,
											   need_ping));
}


void ManyFacedListener::run() {
	m_new_peer->run();
	//m_acceptor.open(m_local_ep.protocol());

	continueAccept();
}


void ManyFacedListener::onConnect(const std::shared_ptr<ISocketChannel> &socket_channel) {
	LOG_D() << "Connection established with " <<
			socket_channel->GetRemoteEndpointString() << " " << socket_channel->GetStringId();
}


void ManyFacedListener::onFinished(const std::shared_ptr<ISocketChannel> &socket_channel) {
	LOG_D() << "Close connection " << socket_channel->GetStringId();
	m_peers.erase(socket_channel->m_id);
}


void ManyFacedListener::onLostConnection(const std::shared_ptr<ISocketChannel> &socket_channel) {
	LOG_D() << "Lost connection " << socket_channel->GetStringId();
	//m_peers.erase(socket_channel->m_id);
	socket_channel->CloseConnecion();
}

void ManyFacedListener::onMessage(const std::shared_ptr<ISocketChannel> &socket_channel,
								  const std::string &data,
								  size_t size,
								  uint16_t pid,
								  uint16_t type) {
	LOG_D() << "Received message from " << socket_channel->GetRemoteEndpointString();
}

void ManyFacedListener::continueAccept() {
	m_acceptor.async_accept(m_new_peer->GetSocket(), [=](const boost::system::error_code &ec) {
		if(ec) {
			if(ec == boost::asio::error::operation_aborted) {
				LOG_D() << "Acceptor was aborted ep = "
						<< m_local_ep.address().to_string()
						<< ":"
						<< m_local_ep.port();
				return;
			}

			LOG_D() << "Acceptor error: "
					<< ec.message()
					<< "; ep = "
					<< m_local_ep.address().to_string()
					<< ":"
					<< m_local_ep.port();

			usleep(BAD_ACCEPT_TIMEOUT_MS * 1000);
			continueAccept();
			return;
		}

		m_new_peer->onConnect();
		m_peers[m_new_peer->m_id] = m_new_peer;
		auto id = m_new_peer->m_id;

		m_new_peer.reset(new ReceiverSocketChannel(m_peers[id]->GetIoService(),
												   this,
												   m_local_ep,
												   m_peer_increment++,
												   m_need_ping));
		m_new_peer->run();
		continueAccept();
	});
}


ManyFacedListener::~ManyFacedListener() {
	LOG_D() << "~ManyFacedListener()";
	for(auto &peer : m_peers) {
		m_peers[peer.first]->CloseConnecion();
		m_peers.erase(peer.first);
	}
}


void ManyFacedListener::SetListenEndpoint(const tcp::endpoint &ep) {
	m_local_ep = ep;
	m_acceptor.cancel();
	m_acceptor.close();
	while(true) {
		try {
			m_acceptor = tcp::acceptor(m_io, m_local_ep);
			break;
		} catch(const boost::system::system_error &err) {
			LOG_D() << "Acceptor error: "
					<< err.what()
					<< "; ep = "
					<< m_local_ep.address().to_string()
					<< ":"
					<< m_local_ep.port();

			usleep(BAD_ACCEPT_TIMEOUT_MS * 1000);
		}
	}

	m_new_peer.reset(new ReceiverSocketChannel(m_acceptor.get_io_service(),
											   this,
											   m_local_ep,
											   m_peer_increment++,
											   m_need_ping));
	run();
}


}