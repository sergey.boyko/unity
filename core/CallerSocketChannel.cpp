//
// Created by bso on 22.10.17.
//

#include "CallerSocketChannel.h"

namespace core {

void CallerSocketChannel::StateConnect::onChanged() {
	//m_ctx->m_socket->open(m_ctx->m_ep.protocol());
	m_ctx->m_socket->async_connect(m_ctx->m_ep, [=](const boost::system::error_code &err) {
		if(err) {
			LOG_D() << "Couldn't connect to "
					<< m_ctx->m_ep.address().to_string();
			m_ctx->setState<StateLostsConnection>();
			return;
		}
		m_ctx->setState<StateWork>(m_ctx->m_socket->get_io_service());
	});
}


bool CallerSocketChannel::StateConnect::checkStateAllowed(const std::string &new_state_name) {
	if(new_state_name == "StateLostConnection" || new_state_name == "StateWork" || new_state_name == "StateDone") {
		return true;
	}
	LOG_E() << "Incorrect change the state from " << m_state_name << " to " << new_state_name;
	m_ctx->setState<StateDone>();
	return false;
}

void CallerSocketChannel::run() {
	LOG_D() << "Start call to the address: "
			<< m_ep.address().to_string()
			<< ':'
			<< m_ep.port();
	setState<StateConnect>();
}
}