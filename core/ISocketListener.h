//
// Created by bso on 08.10.17.
//

#ifndef ALPMAIN_ISOCKETLISTENER_H
#define ALPMAIN_ISOCKETLISTENER_H

#include <memory>

namespace core {

class ISocketChannel;

class ISocketListener {
public:
	virtual void onConnect(const std::shared_ptr<ISocketChannel> &socket_channel) = 0;

	virtual void onMessage(const std::shared_ptr<ISocketChannel> &socket_channel,
						   const std::string &data,
						   size_t size,
						   uint16_t pid,
						   uint16_t type) = 0;

	virtual void onLostConnection(const std::shared_ptr<ISocketChannel> &socket_channel) = 0;

	virtual void onFinished(const std::shared_ptr<ISocketChannel> &socket_channel)= 0;
};

}


#endif //ALPMAIN_ISOCKETLISTENER_H
