//
// Created by bso on 19.11.17.
//

#include <mysql_connection.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/driver.h>
#include <ProgramLog.h>

#include "SingleThreadedSql.h"

namespace core {

SingleThreadedSql::SingleThreadedSql(const std::string &host,
									 const std::string &login,
									 const std::string &password,
									 const std::string &schema):
		IDatabase(host, login, password, schema) {
	try {
		m_driver = get_driver_instance();
	} catch(const sql::SQLException &er) {
		LOG_E() << "Couldn't get the driver: " << er.what();
		exit(1);
	}
}


void SingleThreadedSql::DoRequest(uint32_t request_id, const std::string &request) {
	LOG_D() << "DoRequest()";
	try {
		sql::PreparedStatement *prepared_statement =
				m_connection->prepareStatement(request);
		sql::ResultSet *res = prepared_statement->executeQuery();

		if(!res) {
			m_requests[request_id]->OnFailedRequest();
			m_requests.erase(request_id);
			return;
		}

		OnResponse(request_id, res);
	} catch(const sql::SQLException &er) {
		LOG_E() << er.what();
		m_requests[request_id]->OnFailedRequest();
		m_requests.erase(request_id);
	}
}


SingleThreadedSql::~SingleThreadedSql() {
	//delete m_connection;
}


void SingleThreadedSql::run() {
	LOG_D() << "run()";
	m_connection = m_driver->connect(m_host, m_login, m_password);
	m_connection->setSchema(m_schema);
}

}
