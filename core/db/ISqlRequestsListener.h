//
// Created by bso on 23.11.17.
//

#ifndef UNITY_ISQLREQUESTSLISTENER_H
#define UNITY_ISQLREQUESTSLISTENER_H

#include <boost/asio/detail/shared_ptr.hpp>

namespace core{

class ISqlRequest;

class ISqlRequestsListener{
public:
	virtual void OnResponse(const std::shared_ptr<ISqlRequest> &request) = 0;

	virtual void OnFailed(const std::shared_ptr<ISqlRequest> &request) = 0;
};

}

#endif //UNITY_ISQLREQUESTSLISTENER_H
