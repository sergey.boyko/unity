//
// Created by bso on 12.11.17.
//

#ifndef UNITY_ISQLREQUEST_H
#define UNITY_ISQLREQUEST_H

#include <string>
#include <cppconn/resultset.h>
#include <memory>

namespace core {

class ISqlRequestsListener;

class ISqlRequest: public std::enable_shared_from_this<ISqlRequest> {
public:
	explicit ISqlRequest(const std::shared_ptr<ISqlRequestsListener> &listener);

	~ISqlRequest() = default;

	void OnFailedRequest();

	virtual void OnResponse(sql::ResultSet *result) = 0;

	std::string GetRequest();

	void SetRequest(const std::string &request);

	void SetId(uint32_t id) {
		m_id = id;
	}

protected:
	uint32_t m_id = 0;

	std::string m_request;

	std::shared_ptr<ISqlRequestsListener> m_listener;
};

}

#endif //UNITY_ISQLREQUEST_H
