//
// Created by bso on 19.11.17.
//

#ifndef UNITY_SINGLETHREADEDSQL_H
#define UNITY_SINGLETHREADEDSQL_H

#include <mysql_connection.h>
#include <cppconn/driver.h>

#include "IDatabase.h"

namespace core {

class SingleThreadedSql: public IDatabase {
public:
	explicit SingleThreadedSql(const std::string &host,
							   const std::string &login,
							   const std::string &password,
							   const std::string &schema);

	virtual ~SingleThreadedSql();

	void DoRequest(uint32_t request_id, const std::string &request) final;

	void run() final;

protected:
	sql::Driver *m_driver;
	sql::Connection *m_connection;
};

}

#endif //UNITY_SINGLETHREADEDSQL_H
