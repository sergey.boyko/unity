//
// Created by bso on 24.11.17.
//

#include "ISqlRequest.h"
#include "ISqlRequestsListener.h"

namespace core {

void ISqlRequest::OnFailedRequest() {
	if(m_listener) {
		m_listener->OnFailed(shared_from_this());
	}
}


ISqlRequest::ISqlRequest(const std::shared_ptr<ISqlRequestsListener> &listener):
		m_listener(listener) {
}


std::string ISqlRequest::GetRequest() {
	return m_request;
}


void ISqlRequest::SetRequest(const std::string &request) {
	m_request = request;
}


}