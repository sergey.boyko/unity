//
// Created by bso on 19.11.17.
//

#ifndef UNITY_IDATABASE_H
#define UNITY_IDATABASE_H

#include <string>
#include <memory>
#include <unordered_map>

#include "ISqlRequest.h"

namespace core {

class IDatabase {
public:
	explicit IDatabase(const std::string &host, const std::string &login, const std::string &password,
					   const std::string &schema):
			m_host(host),
			m_login(login),
			m_password(password),
			m_schema(schema),
			m_next_request_id(0) {
	}

	virtual void run() = 0;

	virtual void AddRequest(const std::shared_ptr<ISqlRequest> &request) {
		LOG_D() << "AddRequest()";
		if(m_requests.empty()) {
			m_next_request_id = 0;
		}

		if(m_next_request_id > 4294967290) {
			m_next_request_id = 0;
		}

		m_requests[m_next_request_id] = request;
		DoRequest(m_next_request_id++, request->GetRequest());
	}

	virtual void DoRequest(uint32_t request_id, const std::string &request) = 0;

	void OnResponse(uint32_t request_id, sql::ResultSet *result) {
		if(!m_requests[request_id]) {
			LOG_D() << "Request with id = " << request_id << " do not exists";
			return;
		}
		m_requests[request_id]->OnResponse(result);
		m_requests.erase(request_id);
	}

protected:
	std::string m_host;
	std::string m_login;
	std::string m_password;
	std::string m_schema;

	uint32_t m_next_request_id;

	std::unordered_map<uint32_t, std::shared_ptr<ISqlRequest>> m_requests;
};

}

#endif //UNITY_IDATABASE_H