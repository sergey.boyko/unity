//
// Created by bso on 04.03.18.
//

#ifndef UNITY_POSIXFILEDESCRIPTOR_H
#define UNITY_POSIXFILEDESCRIPTOR_H


#include <boost/asio/io_service.hpp>
#include <boost/asio/local/stream_protocol.hpp>
#include <queue>
#include <google/protobuf/message.h>
#include "IPosixLocalSocketListener.h"
#include "ProtoParser.h"

namespace core {

#define MAX_RECEIVE 4096

class PosixLocalSocket: public std::enable_shared_from_this<PosixLocalSocket> {
public:
	PosixLocalSocket(IPosixLocalSocketListener *listener,
					 boost::asio::io_service &io,
					 boost::asio::local::stream_protocol::socket &socket);

	void run();

	void SendPacket(uint16_t pid, const google::protobuf::Message &message);

	void SendMessage(const std::string &message);

protected:
	void continueRead();

	void continueWrite();

	IPosixLocalSocketListener *m_listener;

	boost::asio::io_service &m_io;
	boost::asio::local::stream_protocol::socket m_local_socket;

	std::array<char, MAX_RECEIVE> m_read_buffer;

	std::queue<std::string> m_write_buffer;

	ProtoParser m_message_parser;
};

}

#endif //UNITY_POSIXFILEDESCRIPTOR_H
