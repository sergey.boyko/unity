//
// Created by bso on 12.10.17.
//

#include <cstring>
#include "ProtoParser.h"
#include "ProgramLog.h"
#include <boost/asio.hpp>

namespace core {


MessageHeader ProtoParser::ParseMessage(const char *data) {
	MessageHeader header {};
	memcpy(&header, data, sizeof(MessageHeader));

	header.m_pid = ntohs(header.m_pid);
	header.m_type = ntohs(header.m_type);
	header.m_size = ntohl(header.m_size);

	return header;
}

bool ProtoParser::CheckReceivedPacket(const char *data, size_t receive_size) {
	if(receive_size < sizeof(MessageHeader)) {
		return false;
	}

	return ParseMessage(data).m_size == receive_size - sizeof(MessageHeader);
}

std::string ProtoParser::InsertHeader(const std::string &data, uint16_t pid, uint16_t type) {
	MessageHeader header {};
	header.m_size = htonl(static_cast<uint32_t>(data.size()));
	header.m_type = htons(type);
	if(type == Protobuf) {
		header.m_pid = htons(pid);
	} else {
		header.m_pid = htons(0);
	}

	std::string result;
	result.append(reinterpret_cast<char *>(&header), sizeof(header));
	result.append(data);

	return result;
}

std::string ProtoParser::EraseHeader(const std::string &data, size_t &size_output, uint16_t &pid, uint16_t &type) {
	auto header = ParseMessage(data.data());

	std::string new_data(data);
	new_data.erase(new_data.begin(), new_data.begin() + sizeof(MessageHeader));

	size_output = header.m_size;
	pid = header.m_pid;
	type = header.m_type;

	new_data.erase(new_data.begin() + size_output, new_data.end());

	return new_data;
}

std::string ProtoParser::EraseHeader(const std::string &data) {
	auto header = ParseMessage(data.data());

	std::string new_data(data);
	new_data.erase(new_data.begin(), new_data.begin() + sizeof(MessageHeader));

	return new_data;
}

}