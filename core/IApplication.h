//
// Created by bso on 21.10.17.
//

#ifndef UNITY_IAPPLICATION_H
#define UNITY_IAPPLICATION_H

#include <boost/asio/io_service.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/bind.hpp>

namespace core {

using namespace boost::asio;

class IApplication {
public:
	IApplication():
			m_sigusr1(m_io, SIGUSR1),
			m_sigusr2(m_io, SIGUSR2) {
		m_sigusr1.async_wait(boost::bind(&IApplication::handleSigusr1, this, _1, _2));
		m_sigusr2.async_wait(boost::bind(&IApplication::handleSigusr2, this, _1, _2));
	}

	virtual void run() = 0;

	virtual void Restart() = 0;

	virtual void Stop() = 0;

	io_service& GetIO();

protected:

	void handleSigusr1(const boost::system::error_code & err, int signal);

	void handleSigusr2(const boost::system::error_code & err, int signal);

	io_service m_io;

	/**
	* SIGUSR1 - restart
	*/
	signal_set m_sigusr1;

	/**
	* SIGUSR2 - stop
	*/
	signal_set m_sigusr2;
};

}

#endif //UNITY_IAPPLICATION_H
