//
// Created by bso on 12.03.18.
//

#include "ManyFacedCaller.h"
#include "CallerSocketChannel.h"

namespace core {

ManyFacedCaller::ManyFacedCaller(boost::asio::io_service &io, const std::vector<tcp::endpoint> addresses):
		m_io(io),
		m_timer(io) {
	for(const auto &ep : addresses) {
		m_peers[m_peer_increment] =
				std::shared_ptr<CallerSocketChannel>(new CallerSocketChannel(m_io,
																			 this,
																			 ep,
																			 m_peer_increment++,
																			 false));
	}
}


void ManyFacedCaller::run() {
	for(auto &peer : m_peers) {
		peer.second->run();
	}

	m_timer.expires_from_now(boost::posix_time::seconds(BAD_ACCEPT_TIMEOUT));
	m_timer.async_wait([=](const boost::system::error_code &ec) {
		for(auto &it : m_peers) {
			if(!it.second->Connected()
			   && it.second->m_state->m_state_name != "StateConnect") {
				it.second->run();
			}
		}
	});
}


void ManyFacedCaller::ClosePeer(uint32_t id) {
	m_peers[id]->CloseConnecion();
	m_peers.erase(id);
}


void ManyFacedCaller::ClosePeer(std::shared_ptr<ISocketChannel> peer) {
	peer->CloseConnecion();
	m_peers.erase(peer->m_id);
}


void ManyFacedCaller::AddPeer(const tcp::endpoint &ep) {
	m_peers[m_peer_increment] =
			std::shared_ptr<CallerSocketChannel>(new CallerSocketChannel(m_io,
																		 this,
																		 ep,
																		 m_peer_increment,
																		 false));
	m_peers[m_peer_increment++]->run();
}


void ManyFacedCaller::Stop() {
	for(auto &peer : m_peers) {
		peer.second->CloseConnecion();
	}

	m_peers.clear();
}


void ManyFacedCaller::onConnect(
		const std::shared_ptr<ISocketChannel> &socket_channel) {
	LOG_D() << "Connection established with "
			<< socket_channel->GetRemoteEndpointString()
			<< " "
			<< socket_channel->GetStringId();
}


void ManyFacedCaller::onMessage(
		const std::shared_ptr<ISocketChannel> &socket_channel,
		const std::string &data,
		size_t size,
		uint16_t pid,
		uint16_t type) {
	LOG_D() << "Received message from " << socket_channel->GetRemoteEndpointString();
}


void ManyFacedCaller::onLostConnection(
		const std::shared_ptr<ISocketChannel> &socket_channel) {
	LOG_D() << "Lost connection " << socket_channel->GetStringId();
	//socket_channel->CloseConnecion();
}


void ManyFacedCaller::onFinished(
		const std::shared_ptr<ISocketChannel> &socket_channel) {
	LOG_D() << "Close connection " << socket_channel->GetStringId();
	m_peers.erase(socket_channel->m_id);
}

}