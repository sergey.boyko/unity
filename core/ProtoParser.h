//
// Created by bso on 12.10.17.
//

#ifndef ALPMAIN_PROTOPARSER_H
#define ALPMAIN_PROTOPARSER_H

#include <cstdint>
#include <string>
#include "CoreEnums.h"

namespace core {

#pragma pack(push, 1)

struct MessageHeader {
	/**
	* Packet id
	*/
	uint16_t m_pid;

	/**
	*
	*/
	uint16_t m_type;

	/**
	* Packet size
	*/
	uint32_t m_size;
};
#pragma pack(pop)


/**
* Protobuf packet parser
*/
class ProtoParser {
public:
	ProtoParser() = default;

	bool CheckReceivedPacket(const char *data, size_t receive_size);

	/**
	* Insert ProtoHeader in Message
	* @param data - Data
	* @param pid - If type!=Protobuf, then pid can be any
	* @param type - Message type
	* @return - Result string
	*/
	std::string InsertHeader(const std::string &data, uint16_t pid, uint16_t type = Protobuf);

	std::string EraseHeader(const std::string &data,
							size_t &size_output,
							uint16_t &pid,
							uint16_t &type);

	std::string EraseHeader(const std::string &data);

	MessageHeader ParseMessage(const char *data);
};

}

#endif //ALPMAIN_PROTOPARSER_H
