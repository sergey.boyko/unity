//
// Created by bso on 04.03.18.
//

#ifndef UNITY_POSIXLOCALSOCKETLISTENER_H
#define UNITY_POSIXLOCALSOCKETLISTENER_H

namespace core {

class PosixLocalSocket;

class IPosixLocalSocketListener {
public:
	virtual void onMessage(const std::shared_ptr<PosixLocalSocket> &socket_channel,
						   const std::string &data,
						   size_t size,
						   uint16_t pid,
						   uint16_t type) = 0;

	virtual void onFinished(const std::shared_ptr<PosixLocalSocket> &socket_channel)= 0;
};

}

#endif //UNITY_POSIXLOCALSOCKETLISTENER_H
