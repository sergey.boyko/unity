//
// Created by bso on 23.10.17.
//

#include "PeerConnection.h"
#include "CallerSocketChannel.h"

namespace core {

PeerConnection::PeerConnection(boost::asio::io_service &io,
							   const boost::asio::ip::tcp::endpoint &ep,
							   bool need_ping = false):
		m_remote_ep(ep),
		m_need_ping(need_ping),
		m_reconnect_timer(io) {
	LOG_D() << "PeerConnection()";
	m_peer.reset(new CallerSocketChannel(io, this, m_remote_ep, 0, need_ping));
}


PeerConnection::PeerConnection(boost::asio::io_service &io, bool need_ping):
		m_need_ping(need_ping),
		m_reconnect_timer(io) {
}


void PeerConnection::run() {
	m_peer->run();
}


void PeerConnection::onConnect(const std::shared_ptr<core::ISocketChannel> &socket_channel) {
	LOG_D() << "Connection established";
}


void PeerConnection::onMessage(const std::shared_ptr<ISocketChannel> &socket_channel, const std::string &data,
							   size_t size,
							   uint16_t pid, uint16_t type) {
}


void PeerConnection::onLostConnection(const std::shared_ptr<core::ISocketChannel> &socket_channel) {
	m_reconnect_timer.expires_from_now(boost::posix_time::seconds(BAD_ACCEPT_TIMEOUT));
	m_reconnect_timer.async_wait([=](const boost::system::error_code &ec) {
		if(ec) {
			return;
		}
		m_peer->run();
	});
}


void PeerConnection::onFinished(const std::shared_ptr<core::ISocketChannel> &socket_channel) {
	m_peer.reset();
}


void PeerConnection::SendPacket(uint16_t pid, const google::protobuf::Message &message) {
	m_peer->SendPacket(pid, message);
}


void PeerConnection::CloseConnection() {
	m_peer->CloseConnecion();
}

}