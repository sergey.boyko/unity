//
// Created by bso on 17.10.17.
//

#ifndef UNITY_ROOMCONTEXT_H
#define UNITY_ROOMCONTEXT_H

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <upack.pb.h>

namespace core {

enum RoomType {
	//Standard room
	OpenRoom = 1,
	//Standard room with password
	PrivateRoom = 2
};

struct UserContext;

struct RoomContext: public std::enable_shared_from_this<RoomContext> {
	RoomContext() = default;

	explicit RoomContext(const upack::RoomInfo &message);

	virtual void FillRoomLists(const upack::RoomInfo &message,
					   const std::unordered_map<uint32_t,
							   std::shared_ptr<UserContext>> &users);

	virtual ~RoomContext() = 0;

	/**
	* Room id
	*/
	uint32_t m_rid;

	/**
	* Room type
	*/
	uint8_t m_type;

	/**
	* Room name
	*/
	std::string m_name;

	//TODO In future description will be stored in a file
	/**
	* Room description
	*/
	std::string m_description;

	/**
	* Main room language
	*/
	std::string m_language;

	/**
	* Room owner id
	*/
	uint32_t m_owner_id = 0;

	/**
	* Room owner
	*/
	std::shared_ptr<UserContext> m_owner;

	/**
	* Room subscribers
	*/
	std::unordered_map<uint32_t, std::shared_ptr<UserContext>> m_subscribers;

	/**
	* Get list of all subscribers
	* @return - vector list of subscribers
	*/
	virtual std::unordered_map<uint32_t, std::shared_ptr<UserContext>> GetSubscribers();

	virtual std::unordered_map<uint32_t, std::shared_ptr<UserContext>> GetBroadcasters();

	void AddSubscriber(std::shared_ptr<UserContext> user);

	void RemoveSubscriber(std::shared_ptr<UserContext> user);

	void RemoveSubscriber(uint32_t uid);

	void FillMessage(upack::RoomInfo *info);
};

}
#endif //UNITY_ROOMCONTEXT_H
