//
// Created by bso on 17.10.17.
//

#include "UserContext.h"
#include "RoomContext.h"

namespace core {

UserContext::UserContext(const upack::UserInfo &message):
		m_uid(message.m_uid()),
		m_username(message.m_username()),
		m_firstname(message.m_firstname()),
		m_secondname(message.m_secondname()),
		m_photo_url(message.m_photo_url()),
		m_language(message.m_language()),
		m_client_type(message.m_client_type()),
		m_auth_timestamp(message.m_auth_timestamp()),
		m_cookie(message.m_cookie()) {
}


void UserContext::FillRoomLists(const upack::UserInfo &message, const std::unordered_map<uint32_t,
		std::shared_ptr<RoomContext>> &rooms) {
	for(auto i : message.m_in_rooms()) {
		auto room = rooms.find(i);
		if(room != rooms.end()) {
			room->second->AddSubscriber(shared_from_this());
		}
		//TODO else room has already been deleted or was not created
	}

	for(auto i : message.m_own_rooms()) {
		auto room = rooms.find(i);
		if(room != rooms.end()) {
			//Owner shouldn't be in the room
			room->second->m_owner = shared_from_this();
			m_own_rooms[room->second->m_rid] = room->second;
		}
	}
}


UserContext::~UserContext() {
	for(const auto &it:m_in_rooms) {
		it.second->RemoveSubscriber(m_uid);
	}
}

void UserContext::FillMessage(upack::UserInfo *info) {
	info->set_m_uid(m_uid);
	info->set_m_username(m_username);
	info->set_m_firstname(m_firstname);
	info->set_m_secondname(m_secondname);
	info->set_m_photo_url(m_photo_url);
	info->set_m_language(m_language);
	info->set_m_client_type(m_client_type);
	info->set_m_auth_timestamp(static_cast<google::protobuf::uint64>(m_auth_timestamp));
	info->set_m_cookie(m_cookie);
	//TODO password

	for(const auto &it:m_own_rooms) {
		info->add_m_own_rooms(it.first);
	}

	for(auto it:m_in_rooms) {
		info->add_m_in_rooms(it.first);
	}
}


}