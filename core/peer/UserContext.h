//
// Created by bso on 17.10.17.
//

#ifndef UNITY_USERCONTEXT_H
#define UNITY_USERCONTEXT_H

#include <cstdint>
#include <unordered_map>
#include <memory>
#include <ctime>
#include <vector>
#include <upack.pb.h>

namespace core {

class RoomContext;

class UserContext: public std::enable_shared_from_this<UserContext> {
public:
	UserContext() = default;

	explicit UserContext(const upack::UserInfo &message);

	virtual ~UserContext();

	virtual void FillRoomLists(const upack::UserInfo &message,
							   const std::unordered_map<uint32_t,
									   std::shared_ptr<RoomContext>> &rooms);

	void FillMessage(upack::UserInfo *info);

	/**
	* On message from User
	* @param data
	* @param size
	* @param pid
	* @param type
	*/
	virtual void onMessage(const std::string &data, size_t size, uint16_t pid, uint16_t type) = 0;

	/**
	* User id
	*/
	uint32_t m_uid = 0;

	/**
	* Username
	*/
	std::string m_username;

	/**
	* First name
	*/
	std::string m_firstname;

	/**
	* Second name
	*/
	std::string m_secondname;

	/**
	* Photo url
	*/
	std::string m_photo_url;

	/**
	* Main language
	*/
	std::string m_language;

	//Current user data

	/**
	* Client type
	*/
	std::string m_client_type;

	/**
	* Auth timestamp
	*/
	std::time_t m_auth_timestamp;

	/**
	* Client password
	* do not fill it member
	*/
	std::string m_password;

	/**
	* Cookie for identify
	*/
	std::string m_cookie;

	/**
	* User-Created Rooms
	*/
	std::unordered_map<uint32_t, std::shared_ptr<RoomContext>> m_own_rooms;

	/**
	* Rooms that are currently being listened to
	*/
	std::unordered_map<uint32_t, std::shared_ptr<RoomContext>> m_in_rooms;
};

}

#endif //UNITY_USERCONTEXT_H
