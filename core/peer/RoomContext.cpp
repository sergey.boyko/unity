//
// Created by bso on 17.10.17.
//

#include <ProgramLog.h>
#include "RoomContext.h"
#include "UserContext.h"

namespace core {

RoomContext::RoomContext(const upack::RoomInfo &message):
		m_rid(message.m_rid()),
		m_type(RoomType::OpenRoom),
		m_name(message.m_name()),
		m_description(message.m_description()),
		m_language(message.m_language()),
		m_owner_id(message.m_owner()) {
	if(message.m_type() != RoomType::OpenRoom) {
		LOG_E() << "Uncorrected RoomType";
	}
}


std::unordered_map<uint32_t, std::shared_ptr<UserContext>> RoomContext::GetSubscribers() {
	return m_subscribers;
}


void RoomContext::AddSubscriber(std::shared_ptr<UserContext> user) {
	m_subscribers[user->m_uid] = user;
	user->m_in_rooms[m_rid] = shared_from_this();

	if(m_owner_id == user->m_uid) {
		m_owner = user;
		user->m_own_rooms[m_rid] = shared_from_this();
	}
}


void RoomContext::RemoveSubscriber(std::shared_ptr<UserContext> user) {
	m_subscribers.erase(user->m_uid);
	user->m_in_rooms.erase(m_rid);
}

void RoomContext::RemoveSubscriber(uint32_t uid) {
	auto it = m_subscribers.find(uid);
	if(it != m_subscribers.end()) {
		it->second->m_in_rooms.erase(m_rid);
		m_subscribers.erase(it);
	}
}

void RoomContext::FillRoomLists(const upack::RoomInfo &message, const std::unordered_map<uint32_t,
		std::shared_ptr<UserContext>> &users) {
	for(unsigned int it : message.m_subscribers()) {
		auto user = users.find(it);
		if(user != users.end()) {
			AddSubscriber(user->second);
		}
		//TODO else room has already been deleted or was not created
	}

	auto user = users.find(message.m_owner());
	if(user != users.end()) {
		m_owner = user->second;
		user->second->m_own_rooms[m_rid] = shared_from_this();
	}
	//TODO else room has already been deleted or was not created
}

std::unordered_map<uint32_t, std::shared_ptr<UserContext>> RoomContext::GetBroadcasters() {
	return m_subscribers;
}

void RoomContext::FillMessage(upack::RoomInfo *info) {
	info->set_m_rid(m_rid);
	info->set_m_type(m_type);
	info->set_m_name(m_name);
	info->set_m_description(m_description);
	info->set_m_language(m_language);
	info->set_m_owner(m_owner_id);

	for(auto it : m_subscribers) {
		info->add_m_subscribers(it.first);
	}

	for(auto it : m_subscribers) {
		info->add_m_subscribers(it.first);
	}
}


}
